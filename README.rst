Scikit-Luc
==========

TLDR;
-----

This is a python package containing the usefull script developed or found by Luc Giffon during his PhD thesis. Those
scripts are mainly related to the machine learning and data processing field.

More informations
-----------------

This package contains

    - A simple API for downloading, loading and perform common operations on some datasets (MNIST and CIFAR10)
    - Some kernel methods for approximation of phi mapping function  (skluc.kernel)
    - Some common operations on tensorflow neural networks (layers, training graph on Mnist)
    - Some common example of various implementation tasks (mainly tensorflow stuff)

Installation from Pypi repository
=================================

Anywhere in your working virtualenv first install tensorflow using the documentation: https://www.tensorflow.org/install/
Then just do:

    pip install scikit-luc

Installation from sources
=========================

In the root directory of the package, just do:

    python setup.py install

or

    pip install -e .


Documentation
=============

See wiki of the git repository. https://gitlab.lis-lab.fr/luc.giffon/scikit-luc/wikis/Introduction


Convention
==========

When some data is supposedly structured (e.g. images should be 2D array), the output of mldataset shouldn't take that
into account by default.