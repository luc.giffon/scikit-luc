import time

# import dill as pickle
import keras
import numpy as np
import numpy.random as rng
from keras import backend as K
from keras.callbacks import TensorBoard
from keras.initializers import he_normal
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Input, Lambda
from keras.layers.normalization import BatchNormalization
from keras.models import Model
from keras.models import Sequential
from keras.optimizers import Adam

from skluc.main.data.mldatasets import OmniglotDataset
from skluc.main.data.transformation.ResizeTransformer import ResizeTransformer
from skluc.main.tensorflow_.utils import batch_generator
from skluc.main.utils import logger


def W_init(shape, name=None):
    values = rng.normal(loc=0, scale=1e-2, size=shape)
    return K.variable(values, name=name)


def b_init(shape, name=None):
    values = rng.normal(loc=0.5, scale=1e-2, size=shape)
    return K.variable(values, name=name)


def pairwise_output_shape(shapes):
    shape1, shape2 = shapes
    return shape1


def build_vgg19_model():
    model = Sequential()

    # Block 1
    model.add(Conv2D(64, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay),
                     kernel_initializer=he_normal(), name='block1_conv1', input_shape=input_shape))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(64, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay),
                     kernel_initializer=he_normal(), name='block1_conv2'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(MaxPooling2D((2, 2), strides=(2, 2), name='block1_pool'))

    # Block 2
    model.add(Conv2D(128, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay),
                     kernel_initializer=he_normal(), name='block2_conv1'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(128, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay),
                     kernel_initializer=he_normal(), name='block2_conv2'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(MaxPooling2D((2, 2), strides=(2, 2), name='block2_pool'))

    # Block 3
    model.add(Conv2D(256, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay),
                     kernel_initializer=he_normal(), name='block3_conv1'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(256, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay),
                     kernel_initializer=he_normal(), name='block3_conv2'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(256, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay),
                     kernel_initializer=he_normal(), name='block3_conv3'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(256, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay),
                     kernel_initializer=he_normal(), name='block3_conv4'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(MaxPooling2D((2, 2), strides=(2, 2), name='block3_pool'))

    # Block 4
    model.add(Conv2D(512, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay),
                     kernel_initializer=he_normal(), name='block4_conv1'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(512, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay),
                     kernel_initializer=he_normal(), name='block4_conv2'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(512, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay),
                     kernel_initializer=he_normal(), name='block4_conv3'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(512, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay),
                     kernel_initializer=he_normal(), name='block4_conv4'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(MaxPooling2D((2, 2), strides=(2, 2), name='block4_pool'))

    # Block 5
    model.add(Conv2D(512, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay),
                     kernel_initializer=he_normal(), name='block5_conv1'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(512, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay),
                     kernel_initializer=he_normal(), name='block5_conv2'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(512, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay),
                     kernel_initializer=he_normal(), name='block5_conv3'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(512, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay),
                     kernel_initializer=he_normal(), name='block5_conv4'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))

    # model modification for cifar-10
    model.add(Flatten(name='flatten'))
    model.add(Dense(512, use_bias=True, kernel_regularizer=keras.regularizers.l2(weight_decay),
                    kernel_initializer=he_normal(), name='fc_cifa10'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Dropout(dropout))
    model.add(
        Dense(512, kernel_regularizer=keras.regularizers.l2(weight_decay), kernel_initializer=he_normal(), name='fc2'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Dropout(dropout))

    return model


def model_definition():
    x1 = Input(input_shape)
    x2 = Input(input_shape)

    convnet_model = build_vgg19_model()

    repr_x1 = convnet_model(x1)
    repr_x2 = convnet_model(x2)

    pairwise_l1_dis = Lambda(lambda x: K.abs(x[0] - x[1]), output_shape=pairwise_output_shape)([repr_x1, repr_x2])
    prediction = Dense(1, activation="sigmoid", bias_initializer=b_init)(pairwise_l1_dis)

    siamese_net = Model(inputs=[x1, x2], outputs=prediction)

    optimizer = Adam(6e-5)
    siamese_net.compile(loss="binary_crossentropy", optimizer=optimizer, metrics=['accuracy'])

    return convnet_model, siamese_net


if __name__ == "__main__":
    logger.debug("Executing file {}".format(__file__))
    input_shape = (28, 28, 1)
    validation_size = 1000
    n_epoch = 200
    batch_size = 128
    eval_every = 10
    nb_pairs = 30000
    weight_decay = 0.0001
    dropout = 0.5

    tensorboard_path = "./siamese_vgg19_28x28_tensorboard"

    convnet_model, siamese_net = model_definition()

    tb_cb = TensorBoard(log_dir=tensorboard_path, histogram_freq=0)
    cbks = [tb_cb]

    data = OmniglotDataset(validation_size=validation_size, seed=0)
    data.load()
    data.normalize()
    data.to_one_hot()
    data.data_astype(np.float32)
    data.labels_astype(np.float32)
    data.to_image()
    resize_trans = ResizeTransformer(data.s_name, output_shape=input_shape[:-1])
    data.apply_transformer(resize_trans)

    same_pairs = data.get_n_pairs(data.train.labels, True, nb_pairs // 2)
    x_same, y_same = [data.train.data[same_pairs[:, 0]], data.train.data[same_pairs[:, 1]]], np.ones(nb_pairs // 2)
    diff_pairs = data.get_n_pairs(data.train.labels, False, nb_pairs // 2)
    x_diff, y_diff = [data.train.data[diff_pairs[:, 0]], data.train.data[diff_pairs[:, 1]]], np.zeros(nb_pairs // 2)

    x_train, y_train = np.array([np.vstack((x_same[0], x_diff[0])), np.vstack((x_same[1], x_diff[1]))]), np.hstack(
        (y_same, y_diff))
    x_train = np.swapaxes(x_train, 0, 1)
    shuffled_indexes = np.random.permutation(len(y_train))

    i = 0
    while i < n_epoch:
        for X_batch, Y_batch in batch_generator(x_train[shuffled_indexes], y_train[shuffled_indexes], batch_size,
                                                False):
            loss = siamese_net.train_on_batch([X_batch[:, 0], X_batch[:, 1]], Y_batch)
        if i % eval_every == 0:
            same_pairs = data.get_n_pairs(data.validation.labels, True, batch_size // 2)
            x_same, y_same = [data.validation.data[same_pairs[:, 0]], data.validation.data[same_pairs[:, 1]]], np.ones(
                batch_size // 2)
            diff_pairs = data.get_n_pairs(data.validation.labels, False, batch_size // 2)
            x_diff, y_diff = [data.validation.data[diff_pairs[:, 0]], data.validation.data[diff_pairs[:, 1]]], np.zeros(
                batch_size // 2)

            x_batch, y_batch = [np.vstack((x_same[0], x_diff[0])), np.vstack((x_same[1], x_diff[1]))], np.hstack(
                (y_same, y_diff))

            result = siamese_net.evaluate(x=x_batch, y=y_batch)
            print("iteration {}/{}, training loss: {}, eval: {}".format(i + 1, n_epoch, str(loss), str(result)))
        i += 1

    timing = time.time()
    siamese_net.save('{}_siamese_vgg19_less_pool_omniglot_28x28_full.h5'.format(timing))
    convnet_model.save('{}_siamese_vgg19_less_pool_omniglot_28x28_conv.h5'.format(timing))
