import time

import numpy as np
from keras import optimizers
from keras.callbacks import LearningRateScheduler
from keras.initializers import he_normal
from keras.layers import Conv2D, MaxPooling2D, Activation, Dense, Flatten
from keras.layers.normalization import BatchNormalization
from keras.models import Sequential
from keras.preprocessing.image import ImageDataGenerator

import skluc.main.data.mldatasets as dataset
from skluc.main.data.transformation.ResizeTransformer import ResizeTransformer
from skluc.main.utils import logger


def scheduler(epoch):
    """
    Function to pass to the "LearningrateScheduler"

    :param epoch:
    :return:
    """
    if epoch < 80:
        return 0.1
    if epoch < 160:
        return 0.01
    return 0.001


def model_definition():
    model = Sequential()

    model.add(Conv2D(64, (5, 5), padding='same', kernel_initializer=he_normal(), input_shape=input_shape))
    model.add(BatchNormalization())
    model.add(MaxPooling2D((2, 2), strides=(2, 2)))
    model.add(Activation('relu'))

    model.add(Conv2D(128, (5, 5), padding='same', kernel_initializer=he_normal()))
    model.add(BatchNormalization())
    model.add(MaxPooling2D((2, 2), strides=(2, 2)))
    model.add(Activation('relu'))


    model.add(Flatten())

    model.add(Dense(120, kernel_initializer=he_normal()))
    model.add(BatchNormalization())
    model.add(Activation('relu'))

    model.add(Dense(84, kernel_initializer=he_normal()))
    model.add(BatchNormalization())
    model.add(Activation('relu'))

    model.add(Dense(num_classes, kernel_initializer=he_normal()))
    model.add(BatchNormalization())
    model.add(Activation('softmax'))

    sgd = optimizers.SGD(lr=.1, momentum=0.9, nesterov=True)
    model.compile(loss='categorical_crossentropy', optimizer=sgd, metrics=['accuracy'])
    return model


if __name__ == "__main__":
    logger.debug("Executing file {}".format(__file__))
    validation_size = 10000
    seed = 0
    num_classes = 964
    batch_size = 128
    epochs = 100
    # dropout = 0.5
    weight_decay = 0.0001
    input_shape = (28, 28, 1)
    log_filepath = r'./lenet_logs/'

    data = dataset.OmniglotDataset(validation_size=1000, seed=seed)
    data.load()
    data.normalize()
    data.to_one_hot()
    data.data_astype(np.float32)
    data.labels_astype(np.float32)
    data.to_image()
    resizetrans = ResizeTransformer(data.s_name, input_shape[:-1])
    data.apply_transformer(resizetrans)
    (x_train, y_train), (x_test, y_test) = data.train, data.test
    x_val, y_val = data.validation

    model = model_definition()

    # tb_cb = TensorBoard(log_dir=log_filepath, histogram_freq=1, write_grads=True, write_images=True)
    change_lr = LearningRateScheduler(scheduler)
    # cbks = [change_lr, tb_cb]
    cbks = [change_lr]

    print('Using real-time data augmentation.')
    datagen = ImageDataGenerator(horizontal_flip=True,
                                 width_shift_range=0.125, height_shift_range=0.125, fill_mode='constant', cval=0.)

    datagen.fit(x_train)

    iterations = int(data.train.data.shape[0] / batch_size)
    model.fit_generator(datagen.flow(x_train, y_train, batch_size=batch_size),
                        steps_per_epoch=iterations,
                        epochs=epochs,
                        callbacks=cbks,
                        validation_data=(x_val, y_val))

    model.save('{}_lenet_omniglot_28x28.h5'.format(time.time()))
    print("Final evaluation on val set: {}".format(model.evaluate(x_val, y_val)))
