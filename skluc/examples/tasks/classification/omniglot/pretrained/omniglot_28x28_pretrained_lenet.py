import time

import numpy as np
from keras import Sequential, optimizers
from keras.initializers import he_normal
from keras.layers import Dense, Activation, BatchNormalization

from skluc.main.data.mldatasets import OmniglotDataset
from skluc.main.data.transformation.LeCunTransformer import LecunTransformer
from skluc.main.data.transformation.ResizeTransformer import ResizeTransformer

if __name__ == "__main__":
    input_shape = (28, 28, 1)
    validation_size = 1000
    n_epoch = 200
    batch_size = 128
    weight_decay = 0.0001
    num_classes = 964

    data = OmniglotDataset(validation_size=validation_size, seed=0)
    data.load()
    data.normalize()
    data.to_one_hot()
    data.data_astype(np.float32)
    data.labels_astype(np.float32)
    data.to_image()
    resize_trans = ResizeTransformer(data.s_name, output_shape=input_shape[:-1])
    data.apply_transformer(resize_trans)
    lenet_transformer = LecunTransformer("omniglot_28x28", cut_layer_name="activation_2")
    data.apply_transformer(lenet_transformer)
    data.normalize()
    data.flatten()

    model = Sequential()

    model.add(Dense(120, kernel_initializer=he_normal(), input_shape=data.train.data[0].shape))
    model.add(BatchNormalization())
    model.add(Activation('relu'))

    model.add(Dense(84, kernel_initializer=he_normal()))
    model.add(BatchNormalization())
    model.add(Activation('relu'))

    model.add(Dense(num_classes, kernel_initializer=he_normal()))
    model.add(BatchNormalization())
    model.add(Activation('softmax'))
    sgd = optimizers.SGD(lr=.1, momentum=0.9, nesterov=True)
    model.compile(loss='categorical_crossentropy', optimizer=sgd, metrics=['accuracy'])

    print('Using real-time data augmentation.')
    x_train, y_train = data.train
    x_test, y_test = data.test
    x_val, y_val = data.validation

    model.fit(x_train, y_train,
              batch_size=batch_size,
              epochs=n_epoch,
              validation_data=(x_val, y_val))

    model.save('{}_{}.h5'.format(time.time(), __file__))
    print("Final evaluation on val set: {}".format(model.evaluate(x_val, y_val)))
