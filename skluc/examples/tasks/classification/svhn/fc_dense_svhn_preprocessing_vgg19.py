import tensorflow as tf
import numpy as np

import skluc.main.data.mldatasets as dataset
from skluc.main.data import VGG19SvhnTransformer
from skluc.main.tensorflow_.utils import fully_connected, classification_cifar, batch_generator
from skluc.main.utils import logger

if __name__ == "__main__":
    VALIDATION_SIZE = 10000
    SEED_TRAIN_VALIDATION = 0
    DENSE_DIM = 512
    NUM_EPOCH = 50
    BATCH_SIZE = 128

    data = dataset.SVHNDataset(validation_size=VALIDATION_SIZE, seed=SEED_TRAIN_VALIDATION)
    data.load()
    data.normalize()
    # data.to_image()
    data.apply_transformer(VGG19SvhnTransformer(cut_layer_name="block5_conv4"))
    data.to_one_hot()
    data.flatten()
    data.data_astype(np.float32)
    data.labels_astype(np.float32)

    X_train, Y_train = data.train
    X_val, Y_val = data.validation
    X_test, Y_test = data.test
    # print(Y_test[:5])
    # exit()

    input_dim, output_dim = X_train.shape[1], Y_train.shape[1]

    x = tf.placeholder(tf.float32, shape=[None, input_dim], name="x")
    y = tf.placeholder(tf.float32, shape=[None, output_dim], name="label")

    with tf.variable_scope("dense_layers"):
        fc_1 = fully_connected(x, DENSE_DIM, act=tf.nn.relu, variable_scope="fc1")

    classif, keep_prob = classification_cifar(fc_1, output_dim)

    # calcul de la loss
    with tf.name_scope("xent"):
        cross_entropy = tf.reduce_mean(
            tf.nn.softmax_cross_entropy_with_logits(labels=y, logits=classif, name="xentropy"),
            name="xentropy_mean")
        tf.summary.scalar('loss-xent', cross_entropy)

    # calcul du gradient
    with tf.name_scope("train"):
        global_step = tf.Variable(0, name="global_step", trainable=False)
        train_optimizer = tf.train.AdamOptimizer(learning_rate=1e-4).minimize(cross_entropy,
                                                                              global_step=global_step)

    # calcul de l'accuracy
    with tf.name_scope("accuracy"):
        predictions = tf.argmax(classif, 1)
        expected_predictions = tf.argmax(y, 1)
        correct_prediction = tf.equal(predictions, expected_predictions)
        accuracy_op = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
        tf.summary.scalar("accuracy", accuracy_op)

    merged_summary = tf.summary.merge_all()

    init = tf.global_variables_initializer()
    # Create a session for running Ops on the Graph.
    # Instantiate a SummaryWriter to output summaries and the Graph.
    summary_writer = tf.summary.FileWriter("debug_benchmark_vgg")

    with tf.Session() as sess:
        summary_writer.add_graph(sess.graph)
        # Initialize all Variable objects
        sess.run(init)
        for i in range(NUM_EPOCH):
            j = 0
            for X_batch, Y_batch in batch_generator(X_train, Y_train, BATCH_SIZE, True):
                feed_dict = {x: X_batch, y: Y_batch, keep_prob: 0.5}
                _, logits, loss, acc = sess.run([train_optimizer, classif, cross_entropy, accuracy_op], feed_dict=feed_dict)
                if j % 100 == 0:
                    logger.debug("epoch: {}/{}; batch: {}/{}; loss: {}; acc: {}; example logit: {}; example Y: {}"
                                 .format(i, NUM_EPOCH, j, int(X_train.shape[0]/BATCH_SIZE), loss, acc, logits[0],
                                         Y_batch[0]))
                    summary_str = sess.run(merged_summary, feed_dict=feed_dict)
                    summary_writer.add_summary(summary_str, j)
                j += 1

        accuracy_test, preds_test, expect_test = sess.run([accuracy_op, predictions, expected_predictions], feed_dict={
            x: X_test, y: Y_test, keep_prob: 1.0})
        logger.debug("Accuracy on test set: {}".format(accuracy_test))
        logger.debug("Predicted values: {}".format(preds_test))
        logger.debug("Expected values: {}".format(expect_test))

        accuracy_val, preds_val, expect_val = sess.run([accuracy_op, predictions, expected_predictions], feed_dict={
            x: X_val, y: Y_val, keep_prob: 1.0})
        logger.debug("Accuracy on val set: {}".format(accuracy_val))
        logger.debug("Predicted values: {}".format(preds_val))
        logger.debug("Expected values: {}".format(expect_val))
