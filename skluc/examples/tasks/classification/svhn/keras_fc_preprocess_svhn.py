import numpy as np
from keras import optimizers
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.callbacks import LearningRateScheduler, TensorBoard
from keras.regularizers import l2
import skluc.main.data.mldatasets as dataset
from skluc.main.data import VGG19SvhnTransformer

batch_size = 128
epochs = 200
iterations = 391
weight_decay = 0.0001

log_filepath = './lenet_dp_da_wd'


def build_model():
    model = Sequential()
    model.add(Dense(1024, activation='relu', kernel_initializer='he_normal', kernel_regularizer=l2(weight_decay), input_shape=(512,)))
    model.add(Dropout(0.4))
    model.add(Dense(10, activation='softmax', kernel_initializer='he_normal', kernel_regularizer=l2(weight_decay)))
    sgd = optimizers.SGD(lr=.001, momentum=0.9, nesterov=True)
    model.compile(loss='categorical_crossentropy', optimizer=sgd, metrics=['accuracy'])
    return model


def scheduler(epoch):
    if epoch <= 60:
        return 0.05
    if epoch <= 120:
        return 0.01
    if epoch <= 160:
        return 0.002
    return 0.0004


if __name__ == '__main__':

    # load data
    validation_size = 10000
    seed = 0
    data = dataset.SVHNDataset(validation_size=validation_size, seed=seed)
    data.load()
    data.apply_transformer(VGG19SvhnTransformer)
    data.normalize()
    data.flatten()
    data.to_one_hot()
    # data.to_image()
    data.data_astype(np.float32)
    data.labels_astype(np.float32)

    (x_train, y_train), (x_test, y_test) = data.train, data.test

    # build network
    model = build_model()
    print(model.summary())

    # set callback
    tb_cb = TensorBoard(log_dir=log_filepath, histogram_freq=0)
    change_lr = LearningRateScheduler(scheduler)
    cbks = [change_lr, tb_cb]


    # start traing
    model.fit(x=x_train,
              y=y_train,
                steps_per_epoch=iterations,
                epochs=epochs,
                callbacks=cbks,
                validation_data=(x_test, y_test))
    # save model
    # model.save('lenet.h5')
    print("Final evaluation on test set: {}".format(model.evaluate(x_test, y_test)))
