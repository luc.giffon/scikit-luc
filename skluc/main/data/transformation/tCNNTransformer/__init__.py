import os

import numpy as np
from keras import Model
from skluc.main.utils import logger, deprecated


@deprecated
class tCNNTransformer:
    """
    Transform text data with textCNN transformer.
    """
    def __init__(self, name, download_dir=os.path.join(os.path.expanduser("~"), "ml_models")):
        self.tcnn_model = None
        self.s_download_dir = os.path.join(download_dir, name)
        super().__init__()

    def transform(self, data, labels):
        # todo rendre ce truc plus genral aux transformers
        model = Model(inputs=self.tcnn_model.input, outputs=self.tcnn_model.output)
        logger.debug("Type fo data to transform: {}".format(type(data)))
        logger.debug("Length of data to transform: {}".format(len(data)))
        logger.debug("Transforming data using pretrained model")
        transformed_data = np.array(model.predict(data)).reshape(-1, *model.output_shape[1:])
        logger.debug("Type of transformed data: {}".format(type(transformed_data)))
        return transformed_data, labels

    def check_model(self):
        raise NotImplementedError
