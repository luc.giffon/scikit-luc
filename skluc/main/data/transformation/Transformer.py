import os


class Transformer:

    MAP_DATA_MODEL = {}

    def __init__(self, data_name, transformation_name, root_download_dir=os.path.join(os.path.expanduser("~"), "ml_models")):
        self.data_name = data_name
        self.transformation_name = transformation_name
        self.root_download_dir = root_download_dir

    def transform(self, data, labels):
        """
        Apply the transformer to the data and labels.

        :param data: the data to transform
        :type data: np.ndarray
        :param labels: the labels to transform
        :type labels: np.ndarray
        :return: np.ndarray, np.ndarray
        """
        raise NotImplementedError

    def check(self):
        return True

    @property
    def name(self):
        return self.transformation_name

    @property
    def s_download_dir(self):
        return os.path.join(os.path.join(self.root_download_dir, self.__class__.__name__), self.data_name)
