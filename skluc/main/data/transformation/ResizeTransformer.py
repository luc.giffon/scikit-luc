import numpy as np
import tensorflow as tf

from skluc.main.data.transformation.Transformer import Transformer
from skluc.main.utils import logger, Singleton


class ResizeTransformer(Transformer, metaclass=Singleton):
    # todo faire une fit methode?
    def __init__(self, data_name, output_shape):
        if len(output_shape) != 2:
            raise AssertionError("Output shape should be 2D and it is {}D: {}".format(len(output_shape), output_shape))

        transformation_name = self.__class__.__name__ + "_" + "{}x{}".format(output_shape[0], output_shape[1])

        super().__init__(data_name=data_name,
                         transformation_name=transformation_name)
        self.output_shape = output_shape

    def transform(self, data, labels):
        if len(data.shape) != 4:
            raise AssertionError("Data shape should be of size 4 (image batch with channel dimension). "
                                 "It is {}: {}. Maybe have you forgotten to reshape it to an image format?"
                                 "".format(len(data.shape), data.shape))
        logger.debug("Start resizing image")
        logger.debug("Shape of data to transform: {}".format(data.shape))
        logger.debug("Expected output shape: {}".format((data.shape[0], *self.output_shape, data.shape[-1])))

        sess = tf.InteractiveSession()
        images_mat = data
        labels = labels

        new_images = tf.image.resize_images(images_mat, self.output_shape).eval()

        logger.debug("Shape data after resize: {}".format(new_images.shape))
        sess.close()
        tf.reset_default_graph()
        return np.array(new_images), labels
