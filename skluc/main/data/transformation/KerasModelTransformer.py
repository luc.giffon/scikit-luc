import os

import numpy as np

from skluc.main.data.transformation.Transformer import Transformer
from skluc.main.utils import check_file_md5, logger


class KerasModelTransformer(Transformer):
    # todo vgg19 et lecun ne changent rien

    MAP_DATA_MODEL = {}

    def __init__(self, data_name, transformation_name):
        super().__init__(data_name, transformation_name)
        self.keras_model = None
        self.load()

    def load(self):
        raise NotImplementedError

    def check_model(self):
        name = os.path.basename(os.path.normpath(self.MAP_DATA_MODEL[self.data_name].url))
        s_file_path = os.path.join(self.s_download_dir, name)
        if os.path.exists(s_file_path) and check_file_md5(s_file_path,
                                                          self.MAP_DATA_MODEL[self.data_name].checksum,
                                                          raise_=False):
            return True
        else:
            return False

    def transform(self, data, labels):
        if len(data.shape) != 4:
            raise AssertionError("Data shape should be of size 4 (image batch with channel dimension). "
                                 "It is {}: {}. Maybe have you forgotten to reshape it to an image format?"
                                 "".format(len(data.shape), data.shape))
        logger.debug("Type of data to transform: {}".format(type(data)))
        logger.debug("Length of data to transform: {}".format(len(data)))
        logger.debug("Transforming data using pretrained model")
        transformed_data = np.array(self.keras_model.predict(data)).reshape(-1, *self.keras_model.output_shape[1:])
        logger.debug("Type of transformed data: {}".format(type(transformed_data)))
        return transformed_data, labels

    def check(self):
        return self.check_model()
