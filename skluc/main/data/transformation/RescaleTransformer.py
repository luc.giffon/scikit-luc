import tensorflow as tf
import numpy as np

from skluc.main.data.transformation.Transformer import Transformer
from skluc.main.utils import logger, Singleton


class RescaleTransformer(Transformer, metaclass=Singleton):
    def __init__(self, data_name, scaling_factor):
        transformation_name = self.__class__.__name__ + "_" + "{}".format(str(scaling_factor).replace(".", "-"))

        super().__init__(data_name=data_name,
                         transformation_name=transformation_name)

        self.rescale_factor = scaling_factor

    def transform(self, data, labels):
        if len(data.shape) != 4:
            raise AssertionError("Data shape should be of size 4 (image batch with channel dimension). "
                                 "It is {}: {}. Maybe have you forgotten to reshape it to an image format?"
                                 "".format(len(data.shape), data.shape))
        logger.debug("Start resizing image")
        logger.debug("Shape of data to transform: {}".format(data.shape))

        sess = tf.InteractiveSession()
        images_mat = data
        float_output_shape = np.multiply(images_mat.shape[1:-1], (self.rescale_factor, self.rescale_factor))
        output_shape = float_output_shape.astype(np.int)
        if not (output_shape == float_output_shape).all():
            logger.warning("Scaling factor doesn't give round dimension output: Input shape is {}, scaling factor is {}, expected output shape (float) is {} and actual output shape (int) will be {}"
                           .format(images_mat.shape[1:-1], self.rescale_factor, float_output_shape, output_shape))
        labels = labels
        logger.debug("Expected output shape: {}".format((data.shape[0], *output_shape, data.shape[-1])))
        new_images = tf.image.resize_images(images_mat, output_shape).eval()
        logger.debug("Shape of data after rescaling: {}".format(new_images.shape))
        sess.close()
        tf.reset_default_graph()
        return np.array(new_images), labels