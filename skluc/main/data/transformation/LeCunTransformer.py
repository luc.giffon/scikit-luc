from keras import Model
from keras.models import load_model

from skluc.main.data.transformation.KerasModelTransformer import KerasModelTransformer
from skluc.main.utils import logger, create_directory, download_data, check_file_md5, DownloadableModel, Singleton


class LecunTransformer(KerasModelTransformer, metaclass=Singleton):
    """
    Uses the lenet network to transform input data.
    """

    MAP_DATA_MODEL = {
        "mnist": DownloadableModel(
            url="https://pageperso.lis-lab.fr/~luc.giffon/models/1524640419.938414_lenet_mnist.h5",
            checksum="527d7235c213278df1d15d3fe685eb5c"),
        "siamese_omniglot_28x28": DownloadableModel(
            url="https://pageperso.lis-lab.fr/~luc.giffon/models/1536239708.891906_siamese_lenet_omniglot_conv.h5",
            checksum="5092edcb0be7b31b808e221afcede3e6"
        ),
        "omniglot_28x28": DownloadableModel(
            url="https://pageperso.lis-lab.fr/~luc.giffon/models/1536750152.6389275_lenet_omniglot_28x28.h5",
            checksum="c4f20b6dae0722234e1ec0bee85e3a4d"
        )
    }

    def __init__(self, data_name, cut_layer_name=None, cut_layer_index=None):
        if data_name not in self.MAP_DATA_MODEL.keys():
            raise ValueError("Unknown data name. Can't load weights")

        if cut_layer_name is None and cut_layer_index is None:
            logger.warning(
                "Cut layer chosen automatically but it eventually will lead to an error in future: index -1 should be specified explicitly")
            cut_layer_index = -1
        if cut_layer_name is not None:
            transformation_name = str(data_name) + "_" + self.__class__.__name__ + "_" + str(cut_layer_name)
        elif cut_layer_index is not None:
            transformation_name = str(data_name) + "_" + self.__class__.__name__ \
                                  + "_" + str(cut_layer_index)
            # todo sauvegarder index / nom dans le meme dossier si c'est les meme
        else:
            raise AttributeError("Cut layer name or cut_layer index must be given to init VGG19Transformer.")
        self.__cut_layer_name = cut_layer_name
        self.__cut_layer_index = cut_layer_index

        self.keras_model = None

        super().__init__(data_name=data_name,
                         transformation_name=transformation_name)

    def load(self):
        create_directory(self.s_download_dir)
        s_model_path = download_data(self.MAP_DATA_MODEL[self.data_name].url, self.s_download_dir)
        check_file_md5(s_model_path, self.MAP_DATA_MODEL[self.data_name].checksum)
        if self.keras_model is None:
            logger.debug("Loading {} model for {} transformation with {} weights".format(self.__class__.__name__,
                                                                                         self.transformation_name,
                                                                                         self.data_name))
            self.keras_model = load_model(s_model_path)

            logger.debug("Layers of model {}".format([l.name for l in self.keras_model.layers]))

            if self.__cut_layer_index is not None:
                cut_layer = self.keras_model.layers[-1]
                self.__cut_layer_name = cut_layer.name
                logger.debug(
                    "Found associated layer {} to layer index {}".format(self.__cut_layer_name, self.__cut_layer_index))

            self.keras_model = Model(inputs=self.keras_model.input,
                                     outputs=self.keras_model.get_layer(name=self.__cut_layer_name).output)

        else:
            logger.debug("Skip loading model {} for {} transformation with {} weights. Already there.".format(
                self.__class__.__name__,
                self.transformation_name,
                self.data_name))
