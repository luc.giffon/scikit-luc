import os
import pickle
import tarfile

import numpy as np

from skluc.main.data.mldatasets.ImageDataset import ImageDataset
from skluc.main.utils import LabeledData
from skluc.main.utils import logger, check_files


class Cifar100FineDataset(ImageDataset):
    HEIGHT = 32
    WIDTH = 32
    DEPTH = 3

    def __init__(self, validation_size=0, seed=None, s_download_dir=None):
        self.__s_url = "https://www.cs.toronto.edu/~kriz/cifar-100-python.tar.gz"
        self.meta = None
        name = "cifar100fine"
        if s_download_dir is not None:
            super().__init__([self.__s_url], name, s_download_dir, validation_size=validation_size, seed=seed)
        else:
            super().__init__([self.__s_url], name, validation_size=validation_size, seed=seed)

        self.__extracted_dirname = os.path.join(self.s_download_dir, "cifar-100-python")
        self.__extracted_files = [
            'train',
            'test',
            'meta'
        ]

        self.__extracted_file_paths = [os.path.join(self.__extracted_dirname, file) for file in self.__extracted_files]

    def get_cifar100_data(self, keyword):
        """
        Get data from the files containing the keyword in their name.

        :param keyword:
        :return:
        """
        full_data = []
        full_labels = []
        for fpath in self.__extracted_file_paths:
            if keyword in fpath.split('/')[-1]:
                with open(fpath, 'rb') as f:
                    pckl_data = pickle.load(f, encoding='bytes')
                    full_data.append(pckl_data[b'data'])
                    full_labels.append(pckl_data[b'fine_labels'])
        final_data = np.vstack(full_data)
        final_label = np.hstack(full_labels)

        return final_data, final_label

    def get_meta(self):
        """
        Get meta data about cifar10 from file.

        :return:
        """
        for fpath in self.__extracted_file_paths:
            if 'meta' in fpath.split('/')[-1]:
                with open(fpath, 'rb') as f:
                    pckl_data = pickle.load(f, encoding='bytes')
                    meta = pckl_data[b'fine_label_names']
        return np.array(meta)

    def read(self):
        targz_file_path = self.l_filepaths[-1]
        if not check_files(self.__extracted_file_paths):
            logger.debug("Extracting {} ...".format(targz_file_path))
            tar = tarfile.open(targz_file_path, "r:gz")
            tar.extractall(path=self.s_download_dir)
        else:
            logger.debug("File {} has already been extracted".format(targz_file_path))

        logger.debug("Get training data of dataset {}".format(self.s_name))
        self._train = LabeledData(*self.get_cifar100_data('train'))

        logger.debug("Get testing data of dataset {}".format(self.s_name))
        self._test = LabeledData(*self.get_cifar100_data('test'))
        self.meta = self.get_meta()

        self._check_validation_size(self._train[0].shape[0])
