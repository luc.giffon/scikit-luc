"""
This module defines the Dataset classes usefull for downloading and loading datasets as numpy.ndarrays.

The currently implemented datasets are:
    - mnist
    - cifar10
    - cifar100
    - svhn
    - moviereview
"""


from skluc.main.data.mldatasets.Cifar100FineDataset import Cifar100FineDataset
from skluc.main.data.mldatasets.Cifar10Dataset import Cifar10Dataset
from skluc.main.data.mldatasets.MnistDataset import MnistDataset
from skluc.main.data.mldatasets.MovieReviewDataset import MovieReviewV1Dataset
from skluc.main.data.mldatasets.OmniglotDataset import OmniglotDataset
from skluc.main.data.mldatasets.SVHNDataset import SVHNDataset


__all__ = ["Cifar10Dataset", "Cifar100FineDataset", "MnistDataset", "OmniglotDataset", "MovieReviewV1Dataset", "SVHNDataset"]

if __name__ == "__main__":
    d = OmniglotDataset(validation_size=10000)
    d.load()
    # print("Before preprocessing")
    # print(d.train.data.shape, d.train.labels.shape)
    # print(d.validation.data.shape, d.validation.labels.shape)
    # print(d.test.data.shape, d.test.labels.shape)
    # # d.apply_transformer(VGG19SvhnTransformer)
    # print("After vgg19 preprocessing")
    # print(d.train.data.shape, d.train.labels.shape)
    # print(d.validation.data.shape, d.validation.labels.shape)
    # print(d.test.data.shape, d.test.labels.shape)
