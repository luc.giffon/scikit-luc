import os

import numpy as np
from sklearn.preprocessing import LabelBinarizer

from skluc.main.utils import LabeledData
from skluc.main.utils import logger, check_files, silentremove, download_data, create_directory


class Dataset(object):
    """
    Abstract class implementing basic methods for Dataset retrieval.
    """
    # data_groups_private will be used to refer to the attributes self._train and self._test via their names
    # as stringes. It is usefull when the same operations must be performed on train and test set
    data_groups_private = ["_train", "_test"]

    def __init__(self, l_url, s_name, s_download_dir=os.path.join(os.path.expanduser("~"), "ml_datasets"),
                 validation_size=0, seed=None):
        self.l_url = l_url
        self.l_filenames = []
        for url in self.l_url:
            splitted_url = url.split("/")
            self.l_filenames.append(splitted_url[-1])

        self.s_name = s_name
        self.s_download_dir = os.path.join(s_download_dir, self.s_name)
        self.l_filepaths = [os.path.join(self.s_download_dir, fname) for fname in self.l_filenames]
        self._train = None
        self._test = None
        self.seed = seed
        self.permuted_index_train = None
        self.permuted_index_test = None
        self.permuted_index_validation = None
        self.validation_size = validation_size

    def reduce_data_size(self, new_size):
        logger.info("Reducing datasize of dataset {} to .".format(self.s_name, new_size))
        kept_indices = self.get_uniform_class_rand_indices_train(new_size)
        self.permuted_index_train = self.permuted_index_train[kept_indices]

    @staticmethod
    def get_bool_idx_label(find_label, labels):
        """
        return the np.array bool where find_label == label in labels
        :param find_label:
        :param labels:
        :return:
        """

        if len(labels.shape) == 1:
            bool_idx_labs = labels == find_label
        elif len(labels.shape) == 2:
            bool_idx_labs = np.all(labels == find_label, axis=-1)
        else:
            raise ValueError(
                "Function get_uniform_class_rand_indices_train has not been intended for others than scalar and vector valued labels")
        return bool_idx_labs

    def get_uniform_class_rand_indices(self, labels, size, shuffle=True):
        """size is the final size not the size / class"""
        logger.debug("Start finding subset indices of size {} with uniform distribution of labels".format(size))
        unique_labels = np.unique(labels, axis=0)
        nb_labels = len(unique_labels)
        nbr_by_label = size // nb_labels
        logger.debug("Need {} (+/- 1) example by label".format(nbr_by_label))
        # return_idx_labels = np.empty(size, dtype=np.int)
        return_idx_labels = []
        copy_idx = 0

        for u_lab in unique_labels:
            bool_idx_labs = self.get_bool_idx_label(u_lab, labels)
            get_nbr = nbr_by_label
            if len(np.where(bool_idx_labs)[0]) < get_nbr:
                raise IndexError(
                    "Found {} example with label {} when {} was asked".format(len(np.where(bool_idx_labs)[0]), u_lab,
                                                                              get_nbr))
            idx_labs = np.where(bool_idx_labs)[0][:get_nbr]
            # return_idx_labels[copy_idx:copy_idx+get_nbr] = idx_labs
            # logger.debug("Found indexes for label {}: {}; length: {}".format(u_lab, idx_labs, len(idx_labs)))
            return_idx_labels.extend(idx_labs)
            copy_idx += get_nbr

        remaining = size - copy_idx
        if remaining > 0:
            logger.debug("After finding equal number ({}) for example by labels (total = {}), "
                         "need to find more examples to reach size {}".format(nbr_by_label, nbr_by_label * nb_labels,
                                                                              size))
            get_nbr = 1
            while remaining > 0:
                u_lab_idx = np.random.choice(len(unique_labels), 1)[0]
                u_lab = unique_labels[u_lab_idx]
                bool_idx_labs = self.get_bool_idx_label(u_lab, labels)

                all_idx_labs = np.where(bool_idx_labs)[0]
                idx_labs_not_already_gotten = np.setdiff1d(all_idx_labs, return_idx_labels)
                if len(idx_labs_not_already_gotten) < get_nbr:
                    raise IndexError(
                        "Found {} example with label {} when {} was asked".format(len(np.where(bool_idx_labs)[0]),
                                                                                  u_lab, get_nbr))
                idx_labs = idx_labs_not_already_gotten[:get_nbr]
                # todo set difference
                # return_idx_labels[copy_idx:copy_idx + get_nbr] = idx_labs
                return_idx_labels.extend(idx_labs)

                remaining -= 1

        if shuffle:
            np.random.seed(self.seed)
            np.random.shuffle(return_idx_labels)

        return np.array(return_idx_labels)

    def get_uniform_class_rand_indices_train(self, size, shuffle=True):
        return self.get_uniform_class_rand_indices(labels=self.train.labels, size=size, shuffle=shuffle)

    def get_uniform_class_rand_indices_validation(self, size, shuffle=True):
        return self.get_uniform_class_rand_indices(labels=self.validation.labels, size=size, shuffle=shuffle)

    @property
    def train(self):
        return LabeledData(data=self._train.data[self.permuted_index_train],
                           labels=self._train.labels[self.permuted_index_train])

    @property
    def test(self):
        return LabeledData(data=self._test.data[self.permuted_index_test],
                           labels=self._test.labels[self.permuted_index_test])

    @property
    def validation(self):
        # todo doesn't work if val size = 0
        return LabeledData(data=self._train.data[self.permuted_index_validation],
                           labels=self._train.labels[self.permuted_index_validation])

    def download(self):
        """
        Download the dataset.

        :return: None
        """
        create_directory(self.s_download_dir)
        if not check_files(self.l_filepaths):
            logger.debug("Files need to be downloaded")
            for s_fname in self.l_filepaths:
                silentremove(s_fname)
            for s_url in self.l_url:
                logger.debug("Downloading file at url: {}".format(s_url))
                s_file_name = s_url.split("/")[-1]
                download_data(s_url, self.s_download_dir, s_file_name)
        else:
            logger.debug("Files {} already exist".format(self.l_filepaths))

    def _check_validation_size(self, data_length):
        if self.validation_size > data_length:
            raise ValueError("The validation set size ({}) is higher than the train set size ({}). " \
                             "Please choose a little validation set size".format(self.validation_size, data_length))
        logger.debug("Validation size < data length ({} < {})".format(self.validation_size, data_length))

    def to_one_hot(self):
        """
        Convert categorical labels to one hot encoding.

        Note: Beware, the information on how the labels are encoded is not stored
        in the data file.

        :return:
        """
        enc = LabelBinarizer()
        enc.fit(self._train.labels)
        logger.info("Apply one hot encoding to dataset {}.".format(self.s_name))
        for kw in self.data_groups_private:
            datlab = getattr(self, kw)
            if len(datlab.labels) == 0:
                logger.debug("No labels found in {} data of {} dataset".format(kw, self.s_name))
                continue
            logger.debug("Apply one hot encoding to {} data of {} dataset".format(kw, self.s_name))
            labels = np.array(enc.transform(datlab.labels))
            data = datlab.data
            setattr(self, kw, LabeledData(data, labels))

    def revert_one_hot(self):
        """
        Convert one hot encoded labels to categorical labels.

        Note: Beware, the information on how the labels are encoded is not stored
        in the data file.

        :return:
        """
        logger.info("Revert one hot encoding to dataset {}.".format(self.s_name))
        for kw in self.data_groups_private:
            datlab = getattr(self, kw)
            if len(datlab.labels) == 0:
                logger.debug("No labels found in {} data of {} dataset".format(kw, self.s_name))
                continue
            logger.debug("Apply one hot encoding to {} data of {} dataset".format(kw, self.s_name))
            labels = np.argmax(datlab.labels, axis=1)
            data = datlab.data
            setattr(self, kw, LabeledData(data, labels))

    def normalize(self):
        """
        Normalize data.

        Feature scaling normalization.

        Note: Beware, the information on whether or not the data has been normalized
        is not stored in the data file. If you call normalize on already normalized
        it won't have any effect though.

        :return:
        """
        logger.info("Apply normalization to data from dataset {}.".format(self.s_name))
        _min = np.min(self.train.data)
        _max = np.max(self.train.data)
        logger.debug(f"Minimum value of train set is {_min}; max is {_max}")

        for kw in self.data_groups_private:
            logger.debug("Apply normalization to {} data of {} dataset.".format(kw, self.s_name))
            datlab = getattr(self, kw)
            if len(datlab.labels) == 0:
                continue
            data = datlab.data
            logger.debug(f"Minimum value of {kw} set before normalization is {np.min(data)}; max is {np.max(data)}")
            data = (data - _min) / (_max - _min)
            logger.debug(f"Minimum value of {kw} set after normalization is {np.min(data)}; max is {np.max(data)}")
            setattr(self, kw, LabeledData(data, datlab.labels))

    def data_astype(self, _type):
        """
        Change data type to _type.

        Note: Beware, the information on the type of the data is not stored in the data file.

        :param _type:
        :return:
        """
        logger.info("Change type of data to {} in the dataset {}.".format(str(_type), self.s_name))
        for kw in self.data_groups_private:
            datlab = getattr(self, kw)
            if len(datlab.labels) == 0:
                continue
            logger.debug("Change type of {} data to {} in the dataset {}.".format(kw, str(_type), self.s_name))
            data = datlab.data
            logger.debug("{} data was of type {}".format(kw, data.dtype))
            data = data.astype(_type)
            logger.debug("{} data is now of type {}".format(kw, data.dtype))
            setattr(self, kw, LabeledData(data, datlab.labels))

    def labels_astype(self, _type):
        """
        Change labels type to _type.

        Not: Beware, the information on the type of the labels is not stored in the data file.

        :param _type:
        :return:
        """
        logger.info("Change type of labels to {} in the dataset {}.".format(str(_type), self.s_name))
        for kw in self.data_groups_private:
            datlab = getattr(self, kw)
            if len(datlab.labels) == 0:
                continue
            labels = datlab.labels
            logger.debug("Change type of {} labels to {} in the dataset {}.".format(kw, str(_type), self.s_name))
            logger.debug("{} labels were of type {}".format(kw, labels.dtype))
            labels = labels.astype(_type)
            logger.debug("{} labels are now of type {}".format(kw, labels.dtype))
            setattr(self, kw, LabeledData(datlab.data, labels))

    def load(self):
        # todo faire une verification generique que le jeu de donné à été chargé lorsque des opérations
        #  sont appliquées aux données
        logger.info("Loading dataset {}".format(self.s_name))
        self.download()
        self.read()
        if self._train is not None:
            logger.debug("Construction of random train indexes (seed: {})".format(self.seed))
            np.random.seed(self.seed)
            # todo -> faire argument shuffle or not
            permut = np.random.permutation(self._train[0].shape[0])
            if self.validation_size > 0:
                self.permuted_index_train = permut[:-self.validation_size]
                self.permuted_index_validation = permut[-self.validation_size:]
            else:
                self.permuted_index_train = permut
                self.permuted_index_validation = np.array([])
        if self._test is not None:
            logger.debug("Construction of random test indexes (seed: {})".format(self.seed))
            logger.debug("Dataset size: {}".format(self._train[0].shape[0]))
            np.random.seed(self.seed)
            self.permuted_index_test = np.random.permutation(self._test[0].shape[0])
        if self._train is None and self._test is None:
            raise Exception("No data loaded at the end of load method.")

    def save_npz(self, npzdir_path=None):
        """
        Save data and labels as their current state to the npz dir path.

        :param npzdir_path:
        :return:
        """
        # todo trouver une solution pour stocker l'état courant?
        if npzdir_path is None:
            npzdir_path = os.path.join(self.s_download_dir, "npzfiles")
        for kw in self.data_groups_private:
            data, labels = getattr(self, kw)
            dict_attr = {kw + "_data": data, kw + "_labels": labels}
            filepath = os.path.join(npzdir_path, kw + ".npz")
            logger.debug("Shape of {} set: {}".format(kw, data.shape))
            logger.debug("Saving {} data to {}".format(kw, filepath))
            np.savez(filepath, **dict_attr)

    def load_npz(self, npzdir_path=None):
        """
        Load data and labels from the npzdir path.

        :param npzdir_path:
        :return:
        """
        if npzdir_path is None:
            npzdir_path = os.path.join(self.s_download_dir, "npzfiles")
        for kw in self.data_groups_private:
            npzfile_path = os.path.join(npzdir_path, kw + ".npz")
            logger.debug("Loading {}".format(npzfile_path))
            npzfile = np.load(npzfile_path)
            data = npzfile[kw + "_data"]
            logger.debug("Shape of {} set: {}".format(kw, data.shape))
            labels = npzfile[kw + "_labels"]
            setattr(self, kw, LabeledData(data=data, labels=labels))

    # --- Abstract methods

    def read(self):
        """
        This method should load dataset in _train and _test attributes.
        :return:
        """
        raise NotImplementedError
