import os
import zipfile

import imageio
import matplotlib.pyplot as plt
import numpy as np

from skluc.main.data.mldatasets.ImageDataset import ImageDataset
from skluc.main.utils import LabeledData, create_directory
from skluc.main.utils import logger, check_files


class OmniglotDataset(ImageDataset):
    # not a good idea to hard code wieght and width since they can change
    HEIGHT = 105
    WIDTH = 105
    DEPTH = 1

    def __init__(self, validation_size=0, seed=None, s_download_dir=None, snell_preprocessing=False):
        """

        :param validation_size:
        :param seed:
        :param s_download_dir:
        :param snell_preprocessing: should the data preprocessing used in prototypical be used on omniglot
        """
        self.__s_url = ["https://github.com/brendenlake/omniglot/raw/master/python/images_background.zip",
                        "https://github.com/brendenlake/omniglot/raw/master/python/images_evaluation.zip"
                        ]
        self.meta = None
        self.__snell_preprocessing = snell_preprocessing
        name = "omniglot"
        if self.__snell_preprocessing:
            name += "_snell"

        if s_download_dir is not None:
            super().__init__(self.__s_url, name, s_download_dir, validation_size=validation_size, seed=seed)
        else:
            super().__init__(self.__s_url, name, validation_size=validation_size, seed=seed)

        self.__extracted_dirs = [
            os.path.join(self.s_download_dir, "images_background"),
            os.path.join(self.s_download_dir, "images_evaluation")
        ]

    def get_n_pairs(self, labels, same_class, n):
        logger.debug("Get {} pairs of {} class.".format(n, "same" if same_class else "different"))
        unique_labels = np.unique(labels, axis=0)
        # return_idx_labels = np.empty(n, dtype=np.int)
        # pairs = [np.zeros((n, self.h, self.w, 1)) for _ in range(2)]
        pairs = np.empty((0, 2), dtype=np.int)
        i = 0
        while i < n:
            if i % 1000 == 0 and i != 0:
                logger.debug("Got {}/{}".format(i + 1, n))
            rand_lab_idx = np.random.choice(len(unique_labels), 1)[0]
            rand_lab = unique_labels[rand_lab_idx]
            bool_idx_labels = self.get_bool_idx_label(rand_lab, labels)
            idx_labs = np.where(bool_idx_labels)[0]
            if same_class:
                rand_pair = np.random.choice(idx_labs, 2, replace=True)
            else:
                idx_labs_diff = np.setdiff1d(np.arange(len(labels)), idx_labs)
                rand_first_elm = np.random.choice(idx_labs, 1)[0]
                rand_second_elm_diff = np.random.choice(idx_labs_diff, 1)[0]
                rand_pair = np.array([rand_first_elm, rand_second_elm_diff])
            pairs = np.vstack((pairs, rand_pair))
            i += 1
        return pairs

    def get_omniglot_data(self, tag):
        data_dirname = "images_" + tag
        data_dirpath = os.path.join(self.s_download_dir, data_dirname)
        class_index = 0
        list_of_images = []
        list_of_labels = []
        for alphabet_name in os.listdir(data_dirpath):
            data_alphabet_dirpath = os.path.join(data_dirpath, alphabet_name)
            for char in os.listdir(data_alphabet_dirpath):
                charname = alphabet_name + "_" + char[-2:]  # exists in case I need the actual labels
                data_char_dirpath = os.path.join(data_alphabet_dirpath, char)
                for char_image_file in os.listdir(data_char_dirpath):
                    char_image_path = os.path.join(data_char_dirpath, char_image_file)
                    im = imageio.imread(char_image_path)
                    list_of_images.append(im.flatten())
                    list_of_labels.append(class_index)

                class_index += 1
        return np.array(list_of_images), np.array(list_of_labels)

    def read(self):
        npzdir_path = os.path.join(self.s_download_dir, "npzfiles")
        lst_npzfile_paths = [os.path.join(npzdir_path, kw + ".npz")
                             for kw in self.data_groups_private]
        create_directory(npzdir_path)
        if check_files(lst_npzfile_paths):
            # case npz files already exist
            logger.debug("Files {} already exists".format(lst_npzfile_paths))
            logger.info("Loading transformed data from files {}".format(lst_npzfile_paths))
            self.load_npz(npzdir_path)

        else:

            if not check_files(self.__extracted_dirs):
                # case zip files dont even exist
                logger.debug("Extracting {} ...".format(self.l_filepaths))
                for zip_file in self.l_filepaths:
                    zip_ref = zipfile.ZipFile(zip_file, 'r')
                    zip_ref.extractall(self.s_download_dir)
                    zip_ref.close()
            else:
                logger.debug("Files {} have already been extracted".format(self.l_filepaths))

            logger.debug("Get training data of dataset {}".format(self.s_name))
            background_data = LabeledData(*self.get_omniglot_data('background'))

            logger.debug("Get testing data of dataset {}".format(self.s_name))
            evaluation_data = LabeledData(*self.get_omniglot_data('evaluation'))

            if self.__snell_preprocessing:
                nb_class_bg_snell = 1200
                unique_labels_train = np.unique(background_data.labels, axis=0)
                nb_labels_train = len(unique_labels_train)
                nb_class_to_move = nb_class_bg_snell - nb_labels_train
                unique_labels_test = np.unique(evaluation_data.labels, axis=0)
                labels_to_move = unique_labels_test[:nb_class_to_move]
                bool_idx_data_to_move = np.zeros(len(evaluation_data.labels), dtype=bool)
                for label in labels_to_move:
                    bool_idx_label = OmniglotDataset.get_bool_idx_label(label, evaluation_data.labels)
                    bool_idx_data_to_move = np.logical_or(bool_idx_data_to_move, bool_idx_label)

                labels_to_add = evaluation_data.labels[bool_idx_data_to_move] + np.max(background_data.labels) + 1
                self._train = LabeledData(data=np.vstack([background_data.data, evaluation_data.data[bool_idx_data_to_move]]),
                                          labels=np.hstack([background_data.labels, labels_to_add ]))
                self._test = LabeledData(data=evaluation_data.data[np.logical_not(bool_idx_data_to_move)],
                                         labels=evaluation_data.labels[np.logical_not(bool_idx_data_to_move)] - nb_class_to_move)
            else:
                self._train = background_data
                self._test = evaluation_data

            self._check_validation_size(self._train[0].shape[0])

            self.save_npz()

        logger.debug("Number of labels in train set {}".format(len(np.unique(self._train.labels, axis=0))))
        logger.debug("Number of labels in evaluation set {}".format(len(np.unique(self._test.labels, axis=0))))


if __name__ == "__main__":
    import time

    d = OmniglotDataset(validation_size=10000)
    d.load()
    exit()
    d.to_image()
    d.normalize()
    for i, im in enumerate(d.train.data):
        print(im.shape)
        im = im.squeeze(axis=2)
        print(im.shape)
        plt.imshow(im)
        plt.show()
        print(d.train.labels[i])
        time.sleep(1)
