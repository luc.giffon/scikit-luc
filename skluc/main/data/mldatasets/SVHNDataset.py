import os

import numpy as np
import scipy.io as sio

from skluc.main.data.mldatasets.ImageDataset import ImageDataset
from skluc.main.utils import LabeledData
from skluc.main.utils import logger


class SVHNDataset(ImageDataset):

    HEIGHT = 32
    WIDTH = 32
    DEPTH = 3

    def __init__(self, validation_size=0, seed=0, s_download_dir=None):
        self.__s_root_url = "http://ufldl.stanford.edu/housenumbers/"
        self.__d_leaf_url = {
            "train_data": "train_32x32.mat",
            "test_data": "test_32x32.mat",
        }

        l_url = [self.__s_root_url + leaf_url for leaf_url in self.__d_leaf_url.values()]
        if s_download_dir is not None:
            super().__init__(l_url, "svhn", s_download_dir, validation_size=validation_size, seed=seed)
        else:
            super().__init__(l_url, "svhn", validation_size=validation_size, seed=seed)

    @staticmethod
    def read_mat(fname):
        """
        loosely copied on https://stackoverflow.com/questions/29185493/read-svhn-dataset-in-python

        Python function for importing the SVHN data set.
        """
        # Load everything in some numpy arrays
        logger.info("Read mat file {}".format(fname))
        data = sio.loadmat(fname)
        img = np.moveaxis(data['X'], -1, 0)
        lbl = data['y']
        return img, lbl

    def read(self):
        """
        Return a dict of data where, for each key is associated a (data, label) tuple.

        The values of the tuple are np.ndarray.

        :return: dict
        """
        # todo add possibility to provide percentage for validation set instead of size
        self._train = LabeledData(
            *self.read_mat(os.path.join(self.s_download_dir, self.__d_leaf_url["train_data"]))
        )

        self._test = LabeledData(
            *self.read_mat(os.path.join(self.s_download_dir, self.__d_leaf_url["test_data"]))
        )

        self._check_validation_size(self._train[0].shape[0])
