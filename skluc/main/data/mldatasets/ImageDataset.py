import os

import numpy as np

from skluc.main.data.mldatasets.Dataset import Dataset
from skluc.main.utils import LabeledData
from skluc.main.utils import logger, create_directory, check_files


class ImageDataset(Dataset):
    HEIGHT = -1
    WIDTH = -1
    DEPTH = -1

    def apply_transformer(self, transformer):
        """

        :param transformer: Transformer object (not a class)
        :return:
        """
        # todo, cette fonction devrait marcher pour tout dataset (donc il faudrait la mettre dans la classe Dataset)
        transformer_name = transformer.transformation_name
        logger.info("Apply transformation {} to data from dataset {} using transformer {}.".format(transformer.__class__.__name__, self.s_name, transformer_name))
        transform_path = os.path.join(self.s_download_dir, transformer_name)
        transform_filepaths = [os.path.join(transform_path, kw + ".npz")
                               for kw in self.data_groups_private]
        create_directory(transform_path)
        if check_files(transform_filepaths) and transformer.check():
            # in the case where the transformations already exist in npz files
            # and the model is the actual good model
            # but I have no guarantee the transformation has been obtained with the stored model though...
            # todo make the npz files to store the md5 checksum of the model that has produced them
            logger.debug("Files {} already exists".format(transform_filepaths))
            logger.info("Loading transformed data from files {}".format(transform_filepaths))
            self.load_npz(transform_path)
        else:
            # in the case the transformations doesn't yet exist
            # one need to apply it to the data
            # then to save the transformation
            logger.debug("Files {} don't exist or model md5 checksum doesn't match. Need to produce them".format(transform_filepaths))
            logger.info("Apply transformation of {} to dataset {}".format(transformer_name, self.s_name))
            for kw in self.data_groups_private:
                data, labels = getattr(self, kw)
                transformed_data, transformed_labels = transformer.transform(data, labels)
                setattr(self, kw, LabeledData(data=transformed_data, labels=transformed_labels))
            self.save_npz(transform_path)

    def to_image(self):
        """
        Modify data to present it like images (matrices) instead of vectors.

        :return: The modified data.
        """
        if self.HEIGHT == -1 or self.WIDTH == -1 or self.DEPTH == -1:
            raise ValueError("Height, width and depth static attributes of class {} should be set.".format(self.__class__))
        for kw in self.data_groups_private:
            datlab = getattr(self, kw)
            if datlab is None:
                continue
            images_vec = datlab.data
            labels = datlab.labels
            length_by_chanel = images_vec.shape[1]/self.DEPTH
            logger.debug("Images vec shape: {}".format(images_vec.shape))
            if int(length_by_chanel) != length_by_chanel:
                raise Exception("Dimensionality problem")
            else:
                length_by_chanel = int(length_by_chanel)
            images_mat = np.reshape(images_vec, (images_vec.shape[0], length_by_chanel, self.DEPTH), order='F')
            images = np.reshape(images_mat, (images_mat.shape[0], self.HEIGHT, self.WIDTH, self.DEPTH), order='C')
            logger.debug("Images mat shape: {}".format(images.shape))
            setattr(self, kw, LabeledData(images, labels))

    def flatten(self):
        """
        Flatten all the datasets (matrices to vectors)

        :return:
        """
        logger.info("Apply flattening to dataset {}.".format(self.s_name))
        for kw in self.data_groups_private:
            logger.debug("Flattening data {} of dataset {}".format(kw, self.s_name))
            datlab = getattr(self, kw)
            init_dim = np.prod([s for s in datlab.data.shape[1:]])
            logger.debug("Shape of {} data: {}".format(kw, datlab.data.shape))
            logger.debug("Number of features in {} data: {}".format(kw, init_dim))
            data = datlab.data.reshape(datlab.data.shape[0], init_dim)
            setattr(self, kw, LabeledData(data=data, labels=datlab.labels))

    def to_feature_vectors(self):
        """
        From a feature representation (W x H x D) of the images, gives the feature vector representation of
        dimension (N x D) with N being W x H.

        :return:
        """
        for kw in self.data_groups_private:
            datlab = getattr(self, kw)
            images_mat = datlab.data
            labels = datlab.labels

            logger.debug("Shape of {} data before reshape: {}".format(kw, images_mat.shape))
            images_mat = images_mat.reshape(images_mat.shape[0], -1, images_mat.shape[-1])
            logger.debug("Shape of {} data after reshape: {}".format(kw, images_mat.shape))
            setattr(self, kw, LabeledData(images_mat, labels))

    def is_image(self):
        return len(self._train.data.shape) > 2 and len(self._test.data.shape) > 2