import os
import re
import tarfile

import numpy as np

from skluc.main.data.mldatasets.Dataset import Dataset
from skluc.main.utils import LabeledData
from skluc.main.utils import create_directory, check_files, logger


class MovieReviewV1Dataset(Dataset):
    data_groups_private = ["_train"]
    TRAIN_SIZE = 9000

    def apply_transformer(self, transformer_class):
        # todo, cette fonction devrait marcher pour tout dataset (donc il faudrait la mettre dans la classe Dataset)
        transformer = transformer_class()
        transformer_name = transformer.__class__.__name__
        transform_path = os.path.join(self.s_download_dir, transformer_name)
        transform_filepaths = [os.path.join(transform_path, kw + ".npz")
                               for kw in self.data_groups_private]
        create_directory(transform_path)
        if check_files(transform_filepaths) and transformer.check_model():
            # in the case where the transformations already exist in npz files
            # and the model is the actual good model
            # but I have no guarantee the transformation has been obtained with the stored model though...
            # todo make the npz files to store the md5 checksum of the model that has produced them
            logger.debug("Files {} already exists".format(transform_filepaths))
            logger.debug("Now load data of files {}".format(transform_filepaths))
            for kw in self.data_groups_private:
                npzfile_path = os.path.join(transform_path, kw + ".npz")
                logger.debug("Loading {}".format(npzfile_path))
                npzfile = np.load(npzfile_path)
                data = npzfile[kw + "_data"]
                labels = npzfile[kw + "_labels"]
                setattr(self, kw, LabeledData(data=data, labels=labels))
            # todo être plus intelligent avec le mode debug du logger. Pour l'instant je met tout en debug
        else:
            # in the case the transformations doesn't yet exist
            # one nead to apply it to the data
            # then to save the transformation
            logger.debug("Files {} don't exist or model md5 checksum doesn't match. Need to produce them".format(transform_filepaths))
            logger.info("Apply convolution of {} to dataset {}".format(transformer_name, self.s_name))
            for kw in self.data_groups_private:
                data, labels = getattr(self, kw)
                transformed_data, transformed_labels = transformer.transform(data, labels)
                setattr(self, kw, LabeledData(data=transformed_data, labels=transformed_labels))
                dict_attr = {kw + "_data": transformed_data, kw + "_labels": transformed_labels}
                filepath = os.path.join(transform_path, kw + ".npz")

                logger.debug("Saving transformed {} data to {}".format(kw, filepath))
                np.savez(filepath, **dict_attr)

    def __init__(self, validation_size=0, seed=0, s_download_dir=None):
        self.__s_url = "http://www.cs.cornell.edu/people/pabo/movie-review-data/rt-polaritydata.tar.gz"

        if s_download_dir is not None:
            super().__init__([self.__s_url], "moviereview", s_download_dir, validation_size=validation_size, seed=seed)
        else:
            super().__init__([self.__s_url], "moviereview", validation_size=validation_size, seed=seed)

        self.__extracted_files = [
            'rt-polarity.pos',
            'rt-polarity.neg'
        ]
        self.__extracted_dirname = os.path.join(self.s_download_dir, "rt-polaritydata")

        self.__extracted_file_paths = [os.path.join(self.__extracted_dirname, file) for file in self.__extracted_files]

        self.__counter = 1
        self.__vocab = {"<pad>": 0}
        self.__reversed_vocab = {0: "<pad>"}

    @property
    def vocab(self):
        return self.__vocab

    @property
    def vocab_inv(self):
        return self.__reversed_vocab

    def read(self):
        # todo faire une fonction d'extraction commune?
        targz_file_path = self.l_filepaths[-1]
        if not check_files(self.__extracted_file_paths):
            logger.debug("Extracting {} ...".format(targz_file_path))
            tar = tarfile.open(targz_file_path, "r:gz")
            tar.extractall(path=self.s_download_dir)
        else:
            logger.debug("File {} has already been extracted".format(targz_file_path))

        data_labeled = MovieReviewV1Dataset.load_data_and_labels(self.__extracted_file_paths[0],
                                                                 self.__extracted_file_paths[1],
                                                                 encoding="ISO-8859-1")

        max_ = -1
        for l in data_labeled[0]:
            max_ = max(max_, len(l.strip().split()))

        lst_arr_ex = []
        for ex in data_labeled[0]:
            splitted_ex = ex.strip().split()
            splitted_ex_nbr = []
            for wrd in splitted_ex:
                if wrd not in self.__vocab:
                    self.__vocab[wrd] = self.__counter
                    self.__reversed_vocab[self.__counter] = wrd
                    self.__counter += 1
                splitted_ex_nbr.append(self.__vocab[wrd])
            arr_splitted_ex_nbr = np.pad(splitted_ex_nbr, (0, max_-len(splitted_ex_nbr)), 'constant',
                                         constant_values=self.__vocab["<pad>"])
            lst_arr_ex.append(np.reshape(arr_splitted_ex_nbr, (1, -1)))
        X = np.concatenate(lst_arr_ex, axis=0)

        self._train = LabeledData(data=X,
                                  labels=data_labeled[1])

    @property
    def train(self):
        # todo no guarantee on the stratification of classes

        indexes = self.permuted_index_train[:self.TRAIN_SIZE - self.validation_size]
        return LabeledData(data=self._train.data[indexes],
                           labels=self._train.labels[indexes])

    @property
    def test(self):
        indexes = self.permuted_index_train[self.TRAIN_SIZE:]
        return LabeledData(data=self._train.data[indexes],
                           labels=self._train.labels[indexes])

    @property
    def validation(self):
        indexes = self.permuted_index_train[(self.TRAIN_SIZE - self.validation_size):self.TRAIN_SIZE]
        return LabeledData(data=self._train.data[indexes],
                           labels=self._train.labels[indexes])

    @property
    def vocabulary_length(self):
        return len(self.__vocab)

    @staticmethod
    def clean_str(string):
        """
        Tokenization/string cleaning for all datasets except for SST.
        Original taken from https://github.com/yoonkim/CNN_sentence/blob/master/process_data.py

        Credit to: https://github.com/dennybritz/cnn-text-classification-tf/blob/master/data_helpers.py
        """
        string = re.sub(r"[^A-Za-z0-9(),!?\'\`]", " ", string)
        string = re.sub(r"\'s", " \'s", string)
        string = re.sub(r"\'ve", " \'ve", string)
        string = re.sub(r"n\'t", " n\'t", string)
        string = re.sub(r"\'re", " \'re", string)
        string = re.sub(r"\'d", " \'d", string)
        string = re.sub(r"\'ll", " \'ll", string)
        string = re.sub(r",", " , ", string)
        string = re.sub(r"!", " ! ", string)
        string = re.sub(r"\(", " \( ", string)
        string = re.sub(r"\)", " \) ", string)
        string = re.sub(r"\?", " \? ", string)
        string = re.sub(r"\s{2,}", " ", string)
        return string.strip().lower()

    @staticmethod
    def load_data_and_labels(positive_data_file, negative_data_file, encoding='utf-8'):
        """
        Loads MR polarity data from files, splits the data into words and generates labels.
        Returns split sentences and labels.

        Credit to: https://github.com/dennybritz/cnn-text-classification-tf/blob/master/data_helpers.py
        """
        # Load data from files
        positive_examples = list(open(positive_data_file, "r", encoding=encoding).readlines())
        positive_examples = [s.strip() for s in positive_examples]
        negative_examples = list(open(negative_data_file, "r", encoding=encoding).readlines())
        negative_examples = [s.strip() for s in negative_examples]
        # Split by words
        x_text = positive_examples + negative_examples
        x_text = [MovieReviewV1Dataset.clean_str(sent) for sent in x_text]
        # Generate labels
        positive_labels = [[0, 1] for _ in positive_examples]
        negative_labels = [[1, 0] for _ in negative_examples]
        y = np.concatenate([positive_labels, negative_labels], 0)
        return LabeledData(data=x_text, labels=y)
