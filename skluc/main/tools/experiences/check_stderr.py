"""
check_stderr: Gather errors of OAR experiments in one file.zipfile.BadZipFile: Bad CRC-32

Usage:
    check_stderr -i IPATH [-p regex] [-P str] [--header] [--verbose] [--commands str]

Options:
    -h --help                   Show this screen.
    -i --input-dir=<IPATH>      Input directory wher to find results
    -p --patern=regex           Specify the pattern of the files to be looked at [default: (.+)_stdout.txt].
    -P --patern-error=regex     Specify the pattern of the error files to be looked at [default: _stderr.txt].
    -r --header                 Says if there is a header in the result files.
    -c --commands=str           Write a file containg the command lines arguments for the scripts that have led to an error
    -v --verbose                Print the lines of the final file
"""

import re
import os

from os import walk
from os.path import isfile, join

import docopt

if __name__ == "__main__":
    arguments = docopt.docopt(__doc__)
    pattern = arguments["--patern"]
    pattern_err = arguments["--patern-error"]
    pattern_cmd = arguments["--commands"]
    path = os.path.abspath(arguments["--input-dir"])

    onlyfiles = []
    for dirpath, dirnames, filenames in walk(path):
        onlyfiles.extend([join(dirpath, f) for f in filenames if isfile(join(dirpath, f))])

    compiled_re = re.compile(pattern)
    if pattern_cmd is not None:
        compiled_re_cmd = re.compile(pattern_cmd)
    errors = []
    command_lines = []
    for f_name in onlyfiles:
        if not compiled_re.match(f_name):
            continue

        with open(f_name, "r") as f:
            str_f = f.read().strip()
            if str_f == "":
                f_name_split = f_name.split("_")
                f_name_base = "_".join(f_name_split[:-1])
                f_name_err = f_name_base + pattern_err
                with open(f_name_err, 'r') as ferr:
                    str_ferr = ferr.read().strip()
                    errors.append(str_ferr)
                    if pattern_cmd is not None:
                        match = compiled_re_cmd.search(str_ferr)
                        str_match = match.group(1).strip()
                        command_lines.append(str_match)

    with open(join(path, "errors.txt"), 'w') as f_out_err:
        for err in errors:
            f_out_err.write(err)
            f_out_err.write("\n\n\n\n")

    if pattern_cmd is not None:
        with open(join(path, "array_param_cmd_errors.txt"), 'w') as f_out_cmd_err:
            for cmd in command_lines:
                f_out_cmd_err.write(cmd)
                f_out_cmd_err.write("\n")