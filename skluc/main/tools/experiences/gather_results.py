"""
gather_results: gather OAR results from one dir to one file called gathered_results in the same directory.

The result files should consist of lines and should have "stdout" in their name.

Usage:
    gather_results -i IPATH [-p regex] [--header] [--verbose]

Options:
    -h --help                   Show this screen.
    -i --input-dir=<IPATH>      Input directory wher to find results
    -p --patern=regex           Specify the pattern of the files to be looked at [default: .+\_stdout.txt].
    -r --header                 Says if there is a header in the result files.
    -v --verbose                Print the lines of the final file
"""

import os
import re
from os import walk
from os.path import isfile, join

import docopt

from skluc.main.utils import logger

if __name__ == '__main__':
    arguments = docopt.docopt(__doc__)
    pattern_to_recognize = arguments["--patern"] #.+\_stdout.txt
    mypath = os.path.abspath(arguments["--input-dir"])
    res_walk = walk(mypath)
    onlyfiles = []
    for dirpath, dirnames, filenames in res_walk:
        onlyfiles.extend([os.path.join(dirpath, f) for f in filenames if isfile(join(dirpath, f))])
    count = 0
    results = []
    compiled_re = re.compile(pattern_to_recognize)
    first_line = ""
    pattern_file_id = r".+\d+job_(\d+)\.\d+_.+"
    compiled_re_file_id = re.compile(pattern_file_id)
    for f_name in onlyfiles:
        if compiled_re.match(f_name) is None:
            continue
        file_id = compiled_re_file_id.match(f_name).group(1)
        with open(f_name, "r") as f:
            lines = f.readlines()
            try:
                if not first_line and arguments["--header"]:
                    first_line = lines[0].strip() + ",file_timestamp"
                if arguments["--header"]:
                    results.append(lines[1].strip() + "," + file_id)
                else:
                    results.append(lines[0].strip() + "," + file_id)
            except IndexError:
                results.append("")

    with open(os.path.join(mypath, "gathered_results.csv"), 'w') as f_w:
        n_full = 0
        n_empty = 0
        if first_line:
            f_w.write(first_line)
            f_w.write("\n")
            if arguments["--verbose"]:
                logger.debug(first_line)
        for s in results:
            f_w.write(s)
            f_w.write("\n")
            if arguments["--verbose"]:
                logger.debug(s)
            if s.strip() != "":
                n_full += 1
            else:
                n_empty += 1
    logger.info("{} full lines | {} empty lines".format(n_full, n_empty))
