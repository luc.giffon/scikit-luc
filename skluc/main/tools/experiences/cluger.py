# -*- coding: utf-8 -*-
"""
Cluger

Usage:
    cluger --output-dir OUTPUT_DIR --script SCRIPT [--walltime integer] --array-params PARAMFILE [-m integer] [--dry-run] [-S integer] [--python INTERPRETER] [-H str]

Options:
    -o --output-dir OUTPUT_DIR          The directory path where the output should be stored.
    -P --python INTERPRETER             The python interpreter path.
    -s --script SCRIPT                  The script path to be executed.
    -a --array-params PARAMFILE         The path to a file containing an array of parameters
    -n --dry-run                        Tell the script not to be run but print the command lines instead
    -m --maximum-job integer            Tell how many simultaneous jobs should be launched.
    -H --host str                       The name of the hosts on which to run the scripts separated by commas.
    -S --start integer                  The number of the starting batch [default: 0].
    -t --walltime integer               The time in hour for each job.
"""

import os
import sys

import docopt
import math
import pathlib
import subprocess
import time

from skluc.main.tools.experiences.executioner import process_script_params, run as executioner
from skluc.main.tools.experiences.oarCmdGenerator import oarcmd

if __name__ == '__main__':
    arguments = docopt.docopt(__doc__)
    OUTPUT_DIR = arguments["--output-dir"]
    pathlib.Path(OUTPUT_DIR).mkdir(parents=True, exist_ok=True)
    SCRIPT = arguments["--script"]
    MAX_LINES = int(arguments["--maximum-job"])  if arguments["--maximum-job"] is not None else math.inf
    DRY_RUN = arguments["--dry-run"]
    INTERPRETER = arguments["--python"]
    HOST = [h.strip() for h in arguments["--host"].split(",")] if arguments["--host"] is not None else None
    START_LINE = int(arguments["--start"])
    TIME = int(arguments["--walltime"])

    LST_PARAMS = process_script_params(None,
                                       os.path.abspath(arguments["--array-params"]) if arguments["--array-params"] is not None else None)

    hash = hash(time.time())

    length_lst_params_to_do = len(LST_PARAMS) - START_LINE

    if length_lst_params_to_do < MAX_LINES:
        executioner(arguments["--python"], OUTPUT_DIR, SCRIPT, LST_PARAMS, dry_run=arguments["--dry-run"], inner=False,
                    hash="", host=HOST, walltime=TIME)
    else:
        nb_pack = length_lst_params_to_do // MAX_LINES
        remaining = length_lst_params_to_do % MAX_LINES
        i = 0
        while i < nb_pack:
            start_value = START_LINE + i * MAX_LINES

            executioner_cmd_line = [str(sys.executable), "executioner.py",
                                    "--output-dir", OUTPUT_DIR,
                                    "--array-params", arguments["--array-params"],
                                    "--script", SCRIPT,
                                    "--start", str(start_value),
                                    "--job-number", str(MAX_LINES),
                                    "--inner",
                                    "--hash", str(hash),
                                    ]
            executioner_cmd_line += ["--dry-run"] if DRY_RUN else []
            executioner_cmd_line += ["--python {}".format(arguments["--python"])] if arguments["--python"] is not None else []
            oar_executioner = oarcmd(gpu=True, command=" ".join(executioner_cmd_line), host=HOST, output_dir=OUTPUT_DIR,
                                     name=str(hash), time=TIME)
            print(" ".join(oar_executioner))
            subprocess_obj = subprocess.Popen(oar_executioner, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            stdout, stderr = subprocess_obj.communicate()
            print(stdout.decode())
            print(stderr.decode())
            i += 1
        if remaining != 0:
            start_value = START_LINE + i * MAX_LINES
            executioner_cmd_line = [str(sys.executable), "executioner.py",
                                    "--output-dir", OUTPUT_DIR,
                                    "--array-params", arguments["--array-params"],
                                    "--script", SCRIPT,
                                    "--start", str(start_value),
                                    "--inner",
                                    "--hash", str(hash),
                                    ]
            executioner_cmd_line += ["--dry-run"] if DRY_RUN else []
            executioner_cmd_line += ["--python {}".format(arguments["--python"])] if arguments[
                                                                                         "--python"] is not None else []
            oar_executioner = oarcmd(gpu=True, command=" ".join(executioner_cmd_line), host=HOST,
                                     output_dir=OUTPUT_DIR, name=str(hash), time=TIME)
            print(" ".join(oar_executioner))
            subprocess_obj = subprocess.Popen(oar_executioner, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            stdout, stderr = subprocess_obj.communicate()
            print(stdout.decode())
            print(stderr.decode())