#!/usr/bin/env python
# coding: utf-8

from __future__ import print_function

import os

"""
 Créé le mercredi  4 mai 2016.

 raccourci oarsubv

"""


def oarcmd(output_dir=None,
           name="OAR",
           gpu=False,
           time=60,
           core=1,
           command='""',
           theano=False,
           besteffort=False,
           host=None,
           cuda=False,
           notify=False,
           mail=None,
           python=False,
           interactive=False,
           anterior=None,
           flags=''):
    """
    Build command line for OAR and print it.

    :param gpu: Boolean, says if the gpu should be used.
    :param time: Integer, number of hours for the walltime of the job.
    :param core: Integer, number of cores to be used.
    :param command: String, the command line to be run by OAR.
    :param theano: Boolean
    :param besteffort: Boolean, says if the job should be runned in besteffort mode.
    :param host: List of Strings, the name of the host on which to run the job.
    :param cuda: Boolean
    :param notify: Boolean, says if the notify option should be used
    :param mail: String, the email to send notification to.
    :param python: Boolean
    :param interactive: Boolean, True for interactive job.
    :param anterior: Integer, the id of the anterior job.
    :param flags: String
    :return:
    """
    p_param = ''
    if gpu:
        p_param += 'gpu IS NOT NULL '
    else:
        p_param += 'gpu IS NULL '

    # todo make it works with multiple possible hosts
    if type(host) == str:
        host = [host]
    if host:
        p_param += "AND (" + " OR ".join(["host LIKE '{}'".format(h) for h in host]) + ") "

    p = ["-p"] + ["({})".format(p_param.strip())]

    l = ["-l"] + ["core={},walltime={}:00:00".format(core, time)]

    n = []
    if notify:
        import getpass
        if mail:
            n = ["--notify"] + ["mail:{}".format(mail)]
        else:
            n = ["--notify"] + ["mail:{}@lis-lab.fr".format(getpass.getuser())]

    t = []
    if besteffort:
        t = ["-t"] + ["besteffort"] + ["-t"] + ["idempotent"]

    a = []
    if anterior is not None:
        a = ["-a"] + ["{}".format(anterior)]

    N = ["-n"] + [name]

    o = []
    if output_dir is not None:
        o = ["-O"] + [os.path.join(output_dir, "%jobname%.%jobid%.stdout")] + \
            ["-E"] + [os.path.join(output_dir, "%jobname%.%jobid%.stderr")]

    if theano:
        # I don't get why flags is set because it will be overwritten by the next "if cuda" and "if python"
        python = True
        cuda = True
        flags = 'THEANO_FLAGS=mode=FAST_RUN,device=gpu0,nvcc.fastmath=True,floatX=float32; {} '.format(flags)

    if cuda:
        flags = 'export PATH=/usr/local/cuda-7.0/bin:$PATH; ' \
                'export LD_LIBRARY_PATH=/usr/local/cuda-7.0/targets/x86_64-linux/lib:$LD_LIBRARY_PATH; {} '.format(
            flags)

    if python:
        flags = 'export PYTHONPATH={}; {}'.format(os.environ["PYTHONPATH"], flags)

    arguments = p + l + n + t + a + N

    if interactive:
        lst_command = ["oarsub"] + ["-I"] + arguments
        # lst_command = [txt for txt in ["oarsub", "-I", p, l, n, t, a] if txt.strip() != ""]
    else:
        script_command = "{} {}".format(flags, command).strip()
        lst_command = ["oarsub"] + arguments + o + [script_command]
        # lst_command = [txt for txt in ["oarsub", p, l, n, t, a, script_command] if txt.strip() != ""]

    # command = " ".join(lst_command)

    return lst_command


def main(run=False,
         gpu=False,
         time=20,
         core=1,
         command='""',
         theano=False,
         besteffort=False,
         host=None,
         cuda=False,
         notify=False,
         mail=None,
         python=False,
         interactive=False,
         anterior=None,
         flags=''):
    """
    :param run: Boolean, run the actual command line if True.

    :param args: see oarcmd positional args
    :param kwargs: see oarcmd keyword args
    :return: None
    """
    raise NotImplementedError()

    cmd_line = oarcmd(gpu=False,
                      time=20,
                      core=1,
                      command='""',
                      theano=False,
                      besteffort=False,
                      host=None,
                      cuda=False,
                      notify=False,
                      mail=None,
                      python=False,
                      interactive=False,
                      anterior=None,
                      flags='')
    print(cmd_line)
    if run:
        os.system(cmd_line)
