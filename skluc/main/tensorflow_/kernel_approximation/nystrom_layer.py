"""
Convnet with nystrom approximation of the feature map.

"""
import time as t

import numpy as np
import tensorflow as tf
from sklearn.metrics.pairwise import rbf_kernel, linear_kernel, additive_chi2_kernel, chi2_kernel, laplacian_kernel

import skluc.main.data.mldatasets as dataset
from skluc.main.tensorflow_.kernel import tf_rbf_kernel, tf_linear_kernel, tf_chi_square_CPD, tf_chi_square_CPD_exp, tf_laplacian_kernel
from skluc.main.tensorflow_.utils import get_next_batch, classification_mnist, convolution_mnist
from skluc.main.utils import logger

tf.logging.set_verbosity(tf.logging.ERROR)


def nystrom_layer(input_x, input_subsample, kernel, W_matrix=None, output_dim=0, output_act=None, **kernel_params):
    """
    Create the deepstrom layer.

    If output_dim is 0, then there is no learnable W created.

    :param input_x:
    :param input_subsample:
    :param output_dim:
    :param kernel:
    :param kernel_params:
    :return:
    """
    nystrom_sample_size = input_subsample.shape[0]
    logger.debug("Nystrom sample size (fct nystrom layer): {}".format(nystrom_sample_size))
    with tf.name_scope("nystrom"):
        init_dim = np.prod([s.value for s in input_x.shape[1:] if s.value is not None])
        logger.debug("Dimention for 1 input example of deepstrom: {}".format(init_dim))
        h_conv_flat = tf.reshape(input_x, [-1, init_dim])
        h_conv_nystrom_subsample_flat = tf.reshape(input_subsample, [nystrom_sample_size, init_dim])
        with tf.name_scope("kernel_vec"):
            kernel_vector = kernel(h_conv_flat, h_conv_nystrom_subsample_flat, **kernel_params)
            kernel_vector = tf.Print(kernel_vector, [kernel_vector[0]], "print op: ", summarize=10)

        if output_dim != 0:
            if W_matrix is None:
                W = tf.get_variable("W_nystrom", [kernel_vector.shape[1], output_dim],
                                    initializer=tf.random_normal_initializer(stddev=0.1))
            else:
                if type(W_matrix) == np.ndarray:
                    W = tf.Variable(initial_value=W_matrix, trainable=False, name="W_nystrom")
                elif type(W_matrix) == tf.Tensor:
                    W = W_matrix
                else:
                    raise ValueError("Unknown type for w_matrix")
            tf.summary.histogram("W_nystrom", W)
            out_fc = tf.matmul(kernel_vector, W)
            tf.summary.histogram("Kernel vector", out_fc)
        else:
            out_fc = kernel_vector
        tf.summary.histogram("output_nystrom", out_fc)
    if output_act is None:
        return out_fc
    else:
        return output_act(out_fc)


def main():
    val_size = 5000
    mnist = dataset.MnistDataset(validation_size=val_size)
    mnist.load()
    mnist.to_one_hot()
    mnist.normalize()
    mnist.data_astype(np.float32)
    mnist.labels_astype(np.float32)

    X_train, Y_train = mnist.train
    X_val, Y_val = mnist.validation
    X_test, Y_test = mnist.test

    NYSTROM_SAMPLE_SIZE = 100
    X_nystrom = X_train[np.random.permutation(NYSTROM_SAMPLE_SIZE)]
    GAMMA = 0.001
    print("Gamma = {}".format(GAMMA))

    with tf.Graph().as_default():
        input_dim, output_dim = X_train.shape[1], Y_train.shape[1]

        x = tf.placeholder(tf.float32, shape=[None, input_dim], name="x")
        x_nystrom = tf.Variable(X_nystrom, name="nystrom_subsample", trainable=False)
        y_ = tf.placeholder(tf.float32, shape=[None, output_dim], name="labels")

        # side size is width or height of the images
        side_size = int(np.sqrt(input_dim))
        x_image = tf.reshape(x, [-1, side_size, side_size, 1])
        x_nystrom_image = tf.reshape(x_nystrom, [NYSTROM_SAMPLE_SIZE, side_size, side_size, 1])
        tf.summary.image("digit", x_image, max_outputs=3)

        # Representation layer
        with tf.variable_scope("convolution_mnist") as scope_conv_mnist:
            h_conv = convolution_mnist(x_image)
            scope_conv_mnist.reuse_variables()
            h_conv_nystrom_subsample = convolution_mnist(x_nystrom_image, trainable=False)

        out_fc = nystrom_layer(h_conv, h_conv_nystrom_subsample, NYSTROM_SAMPLE_SIZE, **{"kernel": tf_rbf_kernel, "gamma": GAMMA})

        y_conv, keep_prob = classification_mnist(out_fc, output_dim=output_dim)

        # # calcul de la loss
        with tf.name_scope("xent"):
            cross_entropy = tf.reduce_mean(
                tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y_conv, name="xentropy"),
                name="xentropy_mean")
            tf.summary.scalar('loss-xent', cross_entropy)

        # # calcul du gradient
        with tf.name_scope("train"):
            global_step = tf.Variable(0, name="global_step", trainable=False)
            train_optimizer = tf.train.AdamOptimizer(learning_rate=1e-4).minimize(cross_entropy, global_step=global_step)

        # # calcul de l'accuracy
        with tf.name_scope("accuracy"):
            predictions = tf.argmax(y_conv, 1)
            correct_prediction = tf.equal(predictions, tf.argmax(y_, 1))
            accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
            tf.summary.scalar("accuracy", accuracy)

        merged_summary = tf.summary.merge_all()

        init = tf.global_variables_initializer()
        # Create a session for running Ops on the Graph.
        sess = tf.Session()
        # Instantiate a SummaryWriter to output summaries and the Graph.
        summary_writer = tf.summary.FileWriter("results_deepfried_stacked")
        summary_writer.add_graph(sess.graph)
        # Initialize all Variable objects
        sess.run(init)
        # actual learning
        started = t.time()
        feed_dict_val = {x: X_val, y_: Y_val, keep_prob: 1.0}
        for i in range(10000):
            X_batch = get_next_batch(X_train, i, 64)
            Y_batch = get_next_batch(Y_train, i, 64)
            feed_dict = {x: X_batch, y_: Y_batch, keep_prob: 0.5}
            # le _ est pour capturer le retour de "train_optimizer" qu'il faut appeler
            # pour calculer le gradient mais dont l'output ne nous interesse pas
            _, loss, y_result, x_exp = sess.run([train_optimizer, cross_entropy, y_conv, x_image], feed_dict=feed_dict)
            if i % 100 == 0:
                print('step {}, loss {} (with dropout)'.format(i, loss))
                r_accuracy = sess.run([accuracy], feed_dict=feed_dict_val)
                print("accuracy: {} on validation set (without dropout).".format(r_accuracy))
                summary_str = sess.run(merged_summary, feed_dict=feed_dict)
                summary_writer.add_summary(summary_str, i)

        stoped = t.time()
        accuracy, preds = sess.run([accuracy, predictions], feed_dict={
            x: X_test, y_: Y_test, keep_prob: 1.0})
        print('test accuracy %g' % accuracy)
        np.set_printoptions(threshold=np.nan)
        print("Prediction sample: " + str(preds[:50]))
        print("Actual values: " + str(np.argmax(Y_test[:50], axis=1)))
        print("Elapsed time: %.4f s" % (stoped - started))


class DeepstromLayer(tf.keras.layers.Layer):
    def __init__(self,
                 subsample,
                 kernel_name,
                 out_dim=None,
                 activation=None,
                 real_nystrom=False,
                 sum_of_kernels=False,
                 stack_of_kernels=False,
                 kernel_dict={}
                 ):

        def init_kernel():
            if kernel_name == "rbf":
                kernel_fct = rbf_kernel
                tf_kernel_fct = tf_rbf_kernel
            elif kernel_name == "linear":
                kernel_fct = linear_kernel
                tf_kernel_fct = tf_linear_kernel
            elif kernel_name == "chi2_cpd":
                kernel_fct = additive_chi2_kernel
                tf_kernel_fct = tf_chi_square_CPD
            elif kernel_name == "chi2_exp_cpd":
                kernel_fct = chi2_kernel
                tf_kernel_fct = tf_chi_square_CPD_exp
            elif kernel_name == "chi2_pd":
                raise NotImplementedError("Bien verifier que ce code ne fait pas bordel")
            elif kernel_name == "laplacian":
                tf_kernel_fct = tf_laplacian_kernel
                kernel_fct = laplacian_kernel
            else:
                raise ValueError("Unknown kernel name: {}".format(kernel_name))
            return kernel_name, kernel_fct, tf_kernel_fct, kernel_dict

        def init_output_dim(subsample_size):
            if out_dim is not None and out_dim > subsample_size:
                logger.debug("Output dim is greater than deepstrom subsample size. Aborting.")
                exit()
            elif out_dim is None:
                return subsample_size
            else:
                return out_dim

        def init_activation():
            if activation == "tan":
                activation_fct = tf.nn.tanh
            elif activation == "relu":
                activation_fct = tf.nn.relu
            else:
                activation_fct = activation

            return activation_fct

        def init_real_nystrom(subsample_size):
            if out_dim != subsample_size and out_dim is not None and real_nystrom:
                logger.warning("If real nystrom is used, the output dim can only be the same as subsample size: "
                                 "{} != {}".format(out_dim, subsample_size))

            return real_nystrom

        def init_subsample():
            return subsample, len(subsample)

        super().__init__()

        self.__subsample, self.__subsample_size = init_subsample()

        self.__sum_of_kernels = sum_of_kernels
        self.__stack_of_kernels = stack_of_kernels

        self.__kernel_name, self.__kernel_fct, self.__tf_kernel_fct, self.__kernel_dict = init_kernel()
        self.__real_nystrom = init_real_nystrom(self.__subsample_size)
        self.__output_dim = init_output_dim(self.__subsample_size)
        self.__activation = init_activation()
        self.__W_matrix = None

        logger.info("Selecting {} deepstrom layer function with "
                    "subsample size = {}, "
                    "output_dim = {}, "
                    "{} activation function "
                    "and kernel = {}"
                    .format("real" if self.__real_nystrom else "learned",
                            self.__subsample_size,
                            self.__output_dim,
                            "with" if self.__activation else "without",
                            self.__kernel_name))

    def build(self, input_shape):
        if self.__output_dim != 0:
            # outputdim == 0 means there is no W matrix and the kernel vector is directly added as input to
            # the next layer
            if self.__real_nystrom:
                W_matrix = None
                logger.debug("Real nystrom asked: eg W projection matrix has the vanilla formula")
                if self.__sum_of_kernels:
                    raise NotImplementedError("This has not been checked for a while. Must be updated.")
                    # here K11 matrix are added before doing nystrom approximation
                    # added_K11 = np.zeros((self.__subsample.shape[0], self.__subsample.shape[0]))
                    # for g_value in GAMMA:  # only rbf kernel is considered
                    #     added_K11 = np.add(added_K11, rbf_kernel(self.__subsample, self.__subsample, gamma=g_value))
                    # U, S, V = np.linalg.svd(added_K11)
                    # invert_root_K11 = np.dot(U / np.sqrt(S), V).astype(np.float32)
                    # W_matrix = stack_K11
                elif self.__stack_of_kernels:
                    raise NotImplementedError("This has not been checked for a while. Must be updated.")
                    # here nystrom approximations are stacked
                    # lst_invert_root_K11 = []
                    # for g_value in GAMMA:
                    #     K11 = rbf_kernel(self.__subsample, self.__subsample, gamma=g_value)
                    #     U, S, V = np.linalg.svd(K11)
                    #     invert_root_K11 = np.dot(U / np.sqrt(S), V).astype(np.float32)
                    #     lst_invert_root_K11.append(invert_root_K11)
                    # stack_K11 = np.vstack(lst_invert_root_K11)
                    # W_matrix = stack_K11
                else:
                    K11 = self.__kernel_fct(self.__subsample, self.__subsample, **self.__kernel_dict)
                    U, S, V = np.linalg.svd(K11)
                    invert_root_K11 = np.dot(U / np.sqrt(S), V).astype(np.float32)
                    W_matrix = invert_root_K11

                self.__W_matrix = self.add_variable(
                    name="W_nystrom",
                    shape=[self.__subsample_size, self.__subsample_size],
                    initializer=lambda *args, **kwargs: tf.Variable(initial_value=W_matrix),
                    trainable=False
                )
            else:
                self.__W_matrix = self.add_variable(
                    name="W_nystrom",
                    shape=[self.__subsample_size, self.__output_dim],
                    initializer=tf.random_normal_initializer(stddev=0.1),
                    trainable=True
                )
        super(DeepstromLayer, self).build(input_shape)

    def call(self, input_x, **kwargs):
        with tf.name_scope("NystromLayer"):
            init_dim = np.prod([s.value for s in input_x.shape[1:] if s.value is not None])
            h_conv_flat = tf.reshape(input_x, [-1, init_dim])
            h_conv_nystrom_subsample_flat = tf.reshape(self.__subsample, [self.__subsample_size, init_dim])
            with tf.name_scope("kernel_vec"):
                kernel_vector = self.__tf_kernel_fct(h_conv_flat, h_conv_nystrom_subsample_flat, **self.__kernel_dict)
                tf.summary.histogram("kernel_vector", kernel_vector)

            if self.__output_dim != 0:
                out = tf.matmul(kernel_vector, self.__W_matrix)
                tf.summary.histogram("W_matrix", self.__W_matrix)
            else:
                out = kernel_vector
        if self.__activation is not None:
            out = self.__activation(out)
        return out

class KernelLayerEndToEnd(tf.keras.layers.Layer):
    def __init__(self,
                 subsample_size,
                 kernel_name,
                 sum_of_kernels=False,
                 stack_of_kernels=False,
                 kernel_dict={}
                 ):

        def init_kernel():
            if kernel_name == "rbf":
                kernel_fct = rbf_kernel
                tf_kernel_fct = tf_rbf_kernel
            elif kernel_name == "linear":
                kernel_fct = linear_kernel
                tf_kernel_fct = tf_linear_kernel
            elif kernel_name == "chi2_cpd":
                kernel_fct = additive_chi2_kernel
                tf_kernel_fct = tf_chi_square_CPD
            elif kernel_name == "chi2_exp_cpd":
                kernel_fct = chi2_kernel
                tf_kernel_fct = tf_chi_square_CPD_exp
            elif kernel_name == "chi2_pd":
                raise NotImplementedError("Bien verifier que ce code ne fait pas bordel")
            elif kernel_name == "laplacian":
                tf_kernel_fct = tf_laplacian_kernel
                kernel_fct = laplacian_kernel
            else:
                raise ValueError("Unknown kernel name: {}".format(kernel_name))
            return kernel_name, kernel_fct, tf_kernel_fct, kernel_dict


        super().__init__()

        self.__subsample_size = subsample_size

        self.__sum_of_kernels = sum_of_kernels
        self.__stack_of_kernels = stack_of_kernels

        self.__kernel_name, self.__kernel_fct, self.__tf_kernel_fct, self.__kernel_dict = init_kernel()

        self.__W_matrix = None

        logger.info("Selecting kernel layer function with "
                    "subsample size = {}, "
                    "and kernel = {}"
                    .format(self.__subsample_size,
                            self.__kernel_name))


    def call(self, inputs, **kwargs):
        if type(inputs) is not list:
            raise ValueError("Inputs of layer deepstrom should be a list")
        if len(inputs[0].shape) != 2:
            raise ValueError(f"Input x should be 2D but it is {len(inputs[0].shape)}D")
        if len(inputs[1].shape) != 2:
            raise ValueError(f"Input subsample should be 2D but it is {len(inputs[1].shape)}D")
        if inputs[1].shape[0] != self.__subsample_size:
            raise ValueError(f"Subsample should be of size {self.__subsample_size}")
        if inputs[0][0].shape[0] != inputs[1][0].shape[0]:
            raise ValueError(f"Input and subsample should have the same dimension")

        input_x = inputs[0]
        input_sub = inputs[1]
        with tf.name_scope("NystromLayer"):
            with tf.name_scope("kernel_vec"):
                kernel_vector = self.__tf_kernel_fct(input_x, input_sub, **self.__kernel_dict)
                logger.debug("Kernel vector computed")
                tf.summary.histogram("kernel_vector", kernel_vector)

        return kernel_vector



class DeepstromLayerEndToEnd(tf.keras.layers.Layer):
    def __init__(self,
                 subsample_size,
                 kernel_name,
                 out_dim=None,
                 activation=None,
                 sum_of_kernels=False,
                 stack_of_kernels=False,
                 kernel_dict={}
                 ):

        def init_kernel():
            if kernel_name == "rbf":
                kernel_fct = rbf_kernel
                tf_kernel_fct = tf_rbf_kernel
            elif kernel_name == "linear":
                kernel_fct = linear_kernel
                tf_kernel_fct = tf_linear_kernel
            elif kernel_name == "chi2_cpd":
                kernel_fct = additive_chi2_kernel
                tf_kernel_fct = tf_chi_square_CPD
            elif kernel_name == "chi2_exp_cpd":
                kernel_fct = chi2_kernel
                tf_kernel_fct = tf_chi_square_CPD_exp
            elif kernel_name == "chi2_pd":
                raise NotImplementedError("Bien verifier que ce code ne fait pas bordel")
            elif kernel_name == "laplacian":
                tf_kernel_fct = tf_laplacian_kernel
                kernel_fct = laplacian_kernel
            else:
                raise ValueError("Unknown kernel name: {}".format(kernel_name))
            return kernel_name, kernel_fct, tf_kernel_fct, kernel_dict

        def init_output_dim(subsample_size):
            if out_dim is not None and out_dim > subsample_size:
                logger.debug("Output dim is greater than deepstrom subsample size. Aborting.")
                exit()
            elif out_dim is None:
                return subsample_size
            else:
                return out_dim

        def init_activation():
            if activation == "tan":
                activation_fct = tf.nn.tanh
            elif activation == "relu":
                activation_fct = tf.nn.relu
            else:
                activation_fct = activation

            return activation_fct

        super().__init__()

        self.__subsample_size = subsample_size

        self.__sum_of_kernels = sum_of_kernels
        self.__stack_of_kernels = stack_of_kernels

        self.__kernel_name, self.__kernel_fct, self.__tf_kernel_fct, self.__kernel_dict = init_kernel()
        self.__output_dim = init_output_dim(self.__subsample_size)
        self.__activation = init_activation()
        self.__W_matrix = None

        logger.info("Selecting deepstrom layer function with "
                    "subsample size = {}, "
                    "output_dim = {}, "
                    "{} activation function "
                    "and kernel = {}"
                    .format(self.__subsample_size,
                            self.__output_dim,
                            "with" if self.__activation else "without",
                            self.__kernel_name))

    def build(self, input_shape):
        if self.__output_dim != 0:
            # outputdim == 0 means there is no W matrix and the kernel vector is directly added as input to
            # the next layer
            self.__W_matrix = self.add_variable(
                name="W_nystrom",
                shape=[self.__subsample_size, self.__output_dim],
                initializer=tf.random_normal_initializer(stddev=0.1),
                trainable=True
            )
        super(DeepstromLayerEndToEnd, self).build(input_shape)

    def call(self, inputs, **kwargs):
        if type(inputs) is not list:
            raise ValueError("Inputs of layer deepstrom should be a list")
        if len(inputs[0].shape) != 2:
            raise ValueError(f"Input x should be 2D but it is {len(inputs[0].shape)}D")
        if len(inputs[1].shape) != 2:
            raise ValueError(f"Input subsample should be 2D but it is {len(inputs[1].shape)}D")
        if inputs[1].shape[0] != self.__subsample_size:
            raise ValueError(f"Subsample should be of size {self.__subsample_size}")
        if inputs[0][0].shape[0] != inputs[1][0].shape[0]:
            raise ValueError(f"Input and subsample should have the same dimension")

        input_x = inputs[0]
        input_sub = inputs[1]
        with tf.name_scope("NystromLayer"):
            with tf.name_scope("kernel_vec"):
                kernel_vector = self.__tf_kernel_fct(input_x, input_sub, **self.__kernel_dict)
                tf.summary.histogram("kernel_vector", kernel_vector)

            if self.__output_dim != 0:
                out = tf.matmul(kernel_vector, self.__W_matrix)
                tf.summary.histogram("W_matrix", self.__W_matrix)
            else:
                out = kernel_vector
        if self.__activation is not None:
            out = self.__activation(out)
        return out


if __name__ == '__main__':
    DeepstromLayerEndToEnd(subsample_size=64,
                           kernel_name='chi2_cpd',
                           kernel_dict={})

