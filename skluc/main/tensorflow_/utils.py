import tensorflow as tf
import numpy as np
# --- Usual functions --- #


def conv2d(x, W):
    return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')


def max_pool(x, pool_size):
    return tf.nn.max_pool(x, ksize=[1, pool_size, pool_size, 1],
                          strides=[1, pool_size, pool_size, 1], padding='SAME')


def get_next_batch(full_set, batch_nbr, batch_size, circle=True):
    """
    Return the next batch of a dataset.

    This function assumes that all the previous batches of this dataset have been taken with the same size.

    If you use a batch_nbr so that start[batch_nbr * batch_size] > full_set.shape[0] then the modulo will be taken
    and the first index will be (batch_nbr * batch_size) % full_set.shape[0].

    :param full_set: the full dataset from which the batch will be taken
    :param batch_nbr: the number of the batch
    :param batch_size: the size of the batch
    :return:
    """
    index_start = (batch_nbr * batch_size) % full_set.shape[0]
    index_stop = ((batch_nbr + 1) * batch_size) % full_set.shape[0]
    if (index_stop > index_start) and (batch_size < full_set.shape[0]):
        return full_set[index_start:index_stop]
    elif circle:
        part1 = full_set[index_start:]
        part2 = full_set[:index_stop]
        return np.vstack((part1, part2))
    else:
        part1 = full_set[index_start:]
        return part1


def batch_generator(X_train, Y_train, batch_size, circle=False):
    """
    Batch generator for one epoch of training.

    Looping of the dataset allows to always give the same batch size so the last batch is filled
    with the first examples of the dataset if it is not of the correct size.

    :param X_train: The whole training set to split into batches
    :param Y_train: The whole training set labels to split into batches
    :param batch_size: The size of each bize to yield
    :param circle: Boolean which says if the dataset should loop
    :return:
    """
    dataset_length = X_train.shape[0]
    j = 0
    while j*batch_size < dataset_length:
        new_X = get_next_batch(X_train, j, batch_size, circle=circle)
        new_Y = get_next_batch(Y_train, j, batch_size, circle=circle)
        min_size = min(new_X.shape[0], new_Y.shape[0])
        # todo faire en sorte de boucler sans toujours reprendre les meme premiers exemples
        # todo ajouter cette fonction (avec get_next_batch) a mldataset
        yield new_X[:min_size], new_Y[:min_size]
        j += 1


def fully_connected(input_op, output_dim, act=None, variable_scope="fully_connected"):
    """
    Implement a layer of size
    :param input_op:
    :param output_dim:
    :param act:
    :param variable_scope:
    :return:
    """
    with tf.variable_scope(variable_scope):
        init_dim = np.prod([s.value for s in input_op.shape if s.value is not None])
        h_pool2_flat = tf.reshape(input_op, [-1, init_dim])
        W_fc1 = tf.get_variable("weights", [init_dim, output_dim], initializer=tf.random_normal_initializer(stddev=0.1))
        b_fc1 = tf.get_variable("biases", [output_dim], initializer=tf.constant_initializer(0.0))
        dense = tf.matmul(h_pool2_flat, W_fc1) + b_fc1
        if act is None:
            result = dense
        else:
            result = act(dense)
        tf.summary.histogram("weights", W_fc1)
        tf.summary.histogram("biases", b_fc1)
    return result


def conv_relu_pool(input_, kernel_shape, bias_shape, pool_size=2, trainable=True, variable_scope="convolution"):
    """
    Generic function for defining a convolutional layer.

    :param input_: The input tensor to be convoluted
    :param kernel_shape: The shape of the kernels/filters
    :param bias_shape: The shape of the bias
    :param variable_scope:
    :return: The output tensor of the convolution
    """
    with tf.variable_scope(variable_scope):
        weights = tf.get_variable("weights", kernel_shape, initializer=tf.random_normal_initializer(stddev=0.1), trainable=trainable)
        biases = tf.get_variable("biases", bias_shape, initializer=tf.constant_initializer(0.0), trainable=trainable)
        tf.summary.histogram("weights", weights)
        tf.summary.histogram("biases", biases)
        conv = conv2d(input_, weights)
        relu = tf.nn.relu(conv + biases)
        tf.summary.histogram("act", relu)
        # in order to reduce dimensionality, use bigger pooling size
        pool = max_pool(relu, pool_size=pool_size)
    return pool


def tf_op(d_feed_dict, ops):
    """
    Simple fct for running tensorflow op.

    :param d_feed_dict:
    :param ops:
    :return:
    """
    sess = tf.Session()
    init = tf.global_variables_initializer()
    sess.run([init])
    sess.run(ops, feed_dict=d_feed_dict)
    sess.close()


def convolution_mnist(input_, trainable=True):
    """
    Define the two convolutionnal layers used for mnist.

    :param input_: The input images to be convoluted
    :param trainable: Say if these convolutional layers should be trained
    :return:
    """
    # initialy, the pool_size was 2 at both convolutions but I used 3 for dimensionality reduction
    with tf.variable_scope("conv_pool_1"):
        conv1 = conv_relu_pool(input_, [5, 5, 1, 20], [20], pool_size=3, trainable=trainable)
    with tf.variable_scope("conv_pool_2"):
        conv2 = conv_relu_pool(conv1, [5, 5, 20, 50], [50], pool_size=3, trainable=trainable)
    return conv2


def convolution_cifar(input_, trainable=True):
    with tf.variable_scope("conv_pool_1"):
        conv1 = conv_relu_pool(input_, [5, 5, 3, 6], [6], pool_size=2, trainable=trainable)
    with tf.variable_scope("conv_pool_2"):
        conv2 = conv_relu_pool(conv1, [5, 5, 6, 16], [16], pool_size=2, trainable=trainable)
    return conv2


def fc_mnist(input_, out_dim):
    """
    Define the fully connected layers used for mnist.

    :param input_: The input tensor
    :return:
    """
    with tf.variable_scope("fc_relu_1"):
        fc = fully_connected(input_, out_dim, act=tf.nn.relu)
    return fc


def fc_cifar(input_, out_dim):
    with tf.variable_scope("fc_relu_1"):
        fc1 = fully_connected(input_, 120, act=tf.nn.relu)
    with tf.variable_scope("fc_relu_2"):
        fc2 = fully_connected(fc1, out_dim, act=tf.nn.relu)
    return fc2


def classification_mnist(input_, output_dim):
    """
    Define the classification layer used for mnist

    :param input_: The input to classify
    :param output_dim: The returned output dimension
    :return:
    """
    # todo remove this useless thing
    with tf.variable_scope("classification_mnist"):
        keep_prob = tf.placeholder(tf.float32, name="keep_prob")
        input_drop = tf.nn.dropout(input_, keep_prob)
        y_ = fully_connected(input_drop, output_dim)
    return y_, keep_prob


def classification_cifar(input_, output_dim=10):
    with tf.variable_scope("classification_cifar"):
        keep_prob = tf.placeholder(tf.float32, name="keep_prob")
        input_drop = tf.nn.dropout(input_, keep_prob)
        y_ = fully_connected(input_drop, output_dim)
    return y_, keep_prob


def inference_mnist(x_image, output_dim):
    """
    Compute inference on the class of the given tensor images.

    :param x_image: Input tensor images
    :param output_dim: The number of class
    :return: The predicted classes
    """
    h_conv = convolution_mnist(x_image)
    # I use output dim as 2048*2 to be comparable with the fastfood approximation implemented in deepfriedconvnet
    out_fc = fc_mnist(h_conv, 2048)
    y_out, keep_prob = classification_mnist(out_fc, output_dim)
    return y_out, keep_prob


def inference_cifar10(x_image, output_dim):
    h_conv = convolution_cifar(x_image)
    out_fc = fc_cifar(h_conv, 2048)
    y_out, keep_prob = classification_cifar(out_fc, output_dim)
    return y_out, keep_prob

# todo verifier que ces random_variable/random_biases sont utilisables
# --- Random Fourier Features --- #


def random_variable(shape, sigma):
    W = np.random.normal(size=shape, scale=sigma).astype(np.float32)
    return tf.Variable(W, name="random_Weights", trainable=False)


def random_biases(shape):
    b = np.random.uniform(0, 2 * np.pi, size=shape).astype(np.float32)
    return tf.Variable(b, name="random_biase", trainable=False)


# --- Representation Layer --- #

def random_features(conv_out, sigma):
    with tf.name_scope("random_features"):
        init_dim = np.prod([s.value for s in conv_out.shape if s.value is not None])
        conv_out2 = tf.reshape(conv_out, [-1, init_dim])

        W = random_variable((init_dim, init_dim), sigma)
        b = random_biases(init_dim)
        h1 = tf.matmul(conv_out2, W, name="Wx") + b
        h1_cos = tf.cos(h1)
        h1_final = tf.scalar_mul(np.sqrt(2.0 / init_dim).astype(np.float32), h1_cos)
        return h1_final


def replace_nan(tensor):
    return tf.where(tf.is_nan(tensor), tf.zeros_like(tensor), tensor)


if __name__ == "__main__":
    X_train = np.arange(12)
    Y_train = np.array(["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p"])
    num_epoch = 5
    batch_size = 2
    circle = True
    for i in range(num_epoch):
        print("new epoch {}".format(i))
        for X_batch, Y_batch in batch_generator(X_train, Y_train, batch_size, circle):
            print(X_batch, Y_batch)
    exit()