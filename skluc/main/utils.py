import errno
import hashlib
import pathlib
import logging
import os
import time as t
import urllib.request
import warnings
from weakref import WeakValueDictionary

import daiquiri
import numpy as np
import psutil
import collections

from sklearn.metrics.pairwise import additive_chi2_kernel
import tensorflow as tf

daiquiri.setup(level=logging.DEBUG)
logger = daiquiri.getLogger()


def time_fct(fct, *args, n_iter=100, **kwargs):
    """
    Return the average time spent by the function.

    :param fct: the actual function to time
    :param args: the positional arguments of the function
    :param n_iter: number of runs of the function
    :param kwargs: the keyword arguments of the function
    :return: the average time of execution of the function
    """
    time_sum = 0
    for _ in range(n_iter):
        start = t.time()
        fct(*args, **kwargs)
        stop = t.time()
        time_sum += stop - start
    return time_sum / n_iter


def log_memory_usage():
    """Logs current memory usage stats.
    See: https://stackoverflow.com/a/15495136

    :return: None
    """
    PROCESS = psutil.Process(os.getpid())
    MEGA = 10 ** 6
    total, available, percent, used, free, _, _, _, _, _ = psutil.virtual_memory()
    total, available, used, free = total / MEGA, available / MEGA, used / MEGA, free / MEGA
    proc = PROCESS.memory_info()[1] / MEGA
    logger.debug('process = %s total = %s available = %s used = %s free = %s percent = %s'
          % (proc, total, available, used, free, percent))

def silentremove(filename):
    """
    Remove filename without raising error if the file doesn't exist.

    :param filename: The filename
    :type filename: str
    :return: None
    """

    try:
        os.remove(filename)
        logger.debug("File {} has been removed".format(filename))
    except OSError as e: # this would be "except OSError, e:" before Python 2.6
        if e.errno != errno.ENOENT: # errno.ENOENT = no such file or directory
            raise # re-raise exception if a different error occurred
        logger.debug("Directory or file {} doesn't exist".format(filename))


def create_directory(_dir, parents=True, exist_ok=True):
    """
    Try to create the directory if it does not exist.

    :param dir: the path to the directory to be created
    :return: None
    """
    logger.debug("Creating directory {} if needed".format(_dir))
    pathlib.Path(_dir).mkdir(parents=parents, exist_ok=exist_ok)


def download_data(url, directory, name=None):
    """
    Download the file at the specified url

    :param url: the end-point url of the need file
    :type url: str
    :param directory: the target directory where to download the file
    :type directory: str
    :param name: the name of the target file downloaded
    :type name: str
    :return: The destination path of the downloaded file
    """
    if name is None:
        name = os.path.basename(os.path.normpath(url))
    s_file_path = os.path.join(directory, name)
    if not os.path.exists(s_file_path):
        urllib.request.urlretrieve(url, s_file_path)
        logger.debug("File {} has been downloaded to {}.".format(url, s_file_path))
    else:
        logger.debug("File {} already exists and doesn't need to be donwloaded".format(s_file_path))

    return s_file_path


def check_file_md5(filepath, md5checksum, raise_=True):
    """
    Check if filepath checksum and md5checksum are the same

    If raise_ == True, raise an exception instead of returning False when md5sum does not correspond.

    :param filepath: The file path to check
    :param md5checksum: The checksum for verification
    :param raise_: Bool says if exception should be raised when md5 doesn't correspond.
    :return: Return True if the md5 are the same
    """
    logger.debug("Check {} md5 checksum with expected checksum {}".format(filepath, md5checksum))
    # Open,close, read file and calculate MD5 on its contents
    with open(filepath, 'rb') as file_to_check:
        # read contents of the file
        data = file_to_check.read()
        # pipe contents of the file through
        md5_returned = hashlib.md5(data).hexdigest()
        logger.debug("Checksum of {} is {}".format(filepath, md5_returned))

    # Finally compare original MD5 with freshly calculated
    if md5checksum == md5_returned:
        logger.debug("Checksum match: file correctly downloaded")
        return True
    else:
        s_not_match = "Checksum of file {}: {} doesn't match the expected checksum {}"\
            .format(filepath, md5_returned, md5checksum)
        if raise_:
            raise ValueError(s_not_match)
        logger.debug(s_not_match)
        return False


def check_files(filepaths):
    logger.debug("Check existence of files {}".format(str(filepaths)))
    return all([os.path.exists(fpath) for fpath in filepaths])


class Singleton(type):
    _instances = WeakValueDictionary()

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            # This variable declaration is required to force a
            # strong reference on the instance.
            instance = super(Singleton, cls).__call__(*args, **kwargs)
            cls._instances[cls] = instance
        return cls._instances[cls]


def singleton(cls):
    """
    Simple singleton implementation.

    Usage:

    @singleton
    class A:
      pass

    a = A()
    b = A()
    # a == b
    """
    instance = None

    def class_instanciation_or_not(*args, **kwargs):
        nonlocal instance
        if not instance:
            instance = cls(*args, **kwargs)
        return instance
    return class_instanciation_or_not


def compute_euristic_sigma(dataset_full, slice_size=1000):
    """
    Given a dataset, return the gamma that should be used (euristically) when using a rbf kernel on this dataset.

    The formula: $\sigma^2 = 1/n^2 * \sum_{i, j}^{n}||x_i - x_j||^2$

    :param dataset: The dataset on which to look for the best sigma
    :return:
    """
    results = []
    dataset_full = np.reshape(dataset_full, (-1, 1))
    if slice_size > dataset_full.shape[0]:
        slice_size = dataset_full.shape[0]
    for i in range(dataset_full.shape[0] // slice_size):
        if (i+1) * slice_size <= dataset_full.shape[0]:
            dataset = dataset_full[i * slice_size: (i+1) * slice_size]
            slice_size_tmp = slice_size
        else:
            dataset = dataset_full[i * slice_size:]
            slice_size_tmp = len(dataset)
        r1 = np.sum(dataset * dataset, axis=1)
        r1 = np.reshape(r1, [-1, 1])
        r2 = np.reshape(r1, [1, -1])
        d_mat = np.dot(dataset, dataset.T)
        d_mat = r1 - 2 * d_mat + r2
        results.append(1/slice_size_tmp**2 * np.sum(d_mat))
    return np.mean(results)


def replace_nan(tensor):
    return np.where(np.isnan(tensor), np.zeros_like(tensor), tensor)


def memory_usage():
    """
    Copy pasted from https://airbrake.io/blog/python-exception-handling/memoryerror

    Prints current memory usage stats.
    See: https://stackoverflow.com/a/15495136

    :return: None
    """
    PROCESS = psutil.Process(os.getpid())
    GIGA = 10 ** 9
    # MEGA_STR = ' ' * MEGA

    mem = psutil.virtual_memory()
    total, available, used, free = mem.total / GIGA, mem.available / GIGA, mem.used / GIGA, mem.free / GIGA
    proc = PROCESS.memory_info()[1] / GIGA
    return 'process = %s total = %s available = %s used = %s free = %s' \
          % (proc, total, available, used, free)


def compute_euristic_sigma_chi2(dataset_full, slice_size=100):
    """
    Given a dataset, return the gamma that should be used (euristically) when using a rbf kernel on this dataset.

    The formula: $\sigma^2 = 1/n^2 * \sum_{i, j}^{n}||x_i - x_j||^2$

    :param dataset: The dataset on which to look for the best sigma
    :return:
    """
    dataset_full = np.reshape(dataset_full, (-1, 1))
    results = []
    if slice_size > dataset_full.shape[0]:
        slice_size = dataset_full.shape[0]
    for i in range(dataset_full.shape[0] // slice_size):
        if (i+1) * slice_size <= dataset_full.shape[0]:
            dataset = dataset_full[i * slice_size: (i+1) * slice_size]
            slice_size_tmp = slice_size
        else:
            dataset = dataset_full[i * slice_size:]
            slice_size_tmp = len(dataset)
        # wall = np.expand_dims(dataset, axis=1)
        # # the drawing of the matrix Y expanded looks like a floor
        # floor = np.expand_dims(dataset, axis=0)
        # numerator = np.square((wall - floor))
        # denominator = wall + floor
        # quotient = numerator / denominator
        # quotient_without_nan = replace_nan(quotient)
        quotient_without_nan = additive_chi2_kernel(dataset)
        results.append(1/slice_size_tmp**2 * np.sum(quotient_without_nan))
        logger.debug("Compute sigma chi2; current mean: {}".format(np.mean(results)))
    return np.mean(results)


def deprecated(msg=""):
    """
    Decorator which can be used to mark functions as deprecated and write a message.

    :param msg: The message you need to write.
    :return: The inner decorator function
    """
    def inner(func):
        """
        This is a decorator which can be used to mark functions
        as deprecated. It will result in a warning being emitted
        when the function is used.
        """
        def new_func(*args, **kwargs):
            s = "Call to deprecated function {}".format(func.__name__)
            if str(msg).strip != "":
                s += ": {}.".format(msg)
            else:
                s += "."
            warnings.warn(s)
            return func(*args, **kwargs)

        new_func.__name__ = func.__name__
        new_func.__doc__ = func.__doc__
        new_func.__dict__.update(func.__dict__)

        return new_func
    return inner


LabeledData = collections.namedtuple("LabeledData", ["data", "labels"])

DownloadableModel = collections.namedtuple("DownloadableModel", ["url", "checksum"])

class DictManager(dict):
    pass
    # def __getattr__(self, item):
    #     return self[item]
    #
    # def __setattr__(self, key, value):
    #     self[key] = value
    #
    # def __delattr__(self, item):
    #     del self[item]

    # def __missing__(self, key):
    #     logger.warning(f"Call to missing key {key} in {self.__class__.__name__}. None value returned.")
    #     self[key] = None
    #     return self[key]

class ParameterManager(DictManager):
    def get_gamma_value(self, dat, chi2=False):
        if self["--gamma"] is None:
            logger.debug("Gamma arguments is None. Need to compute it.")
            if chi2:
                gamma_value = 1. / compute_euristic_sigma_chi2(dat)

            else:
                gamma_value = 1. / compute_euristic_sigma(dat)
        else:
            gamma_value = eval(self["--gamma"])

        logger.debug("Gamma value is {}".format(gamma_value))
        return gamma_value

    def init_kernel(self):
        if self["--rbf-kernel"]:
            return "rbf"
        elif self["--linear-kernel"]:
            return "linear"
        elif self["--chi-square-kernel"]:
            return "chi2_cpd"
        elif self["--exp-chi-square-kernel"]:
            return "chi2_exp_cpd"
        elif self["--chi-square-PD-kernel"]:
            return "chi2_pd"
        elif self["--laplacian-kernel"]:
            return "laplacian"
        else:
            return None

    def init_network(self):
        if self["dense"]:
            return "dense"
        elif self["deepfriedconvnet"]:
            return "deepfriedconvnet"
        elif self["deepstrom"]:
            return "deepstrom"
        elif self["none"]:
            return "none"

    def init_non_linearity(self):
        if self["--non-linearity"] == "tanh":
            return tf.nn.tanh
        elif self["--non-linearity"] == "relu":
            return tf.nn.relu
        elif self["--non-linearity"] == "None":
            return None

    def init_dataset(self):
        if self["--cifar10"]:
            return "cifar10"
        if self["--cifar100"]:
            return "cifar100"
        if self["--mnist"]:
            return "mnist"
        if self["--svhn"]:
            return "svhn"

class ResultManager(DictManager):
    pass

class ResultPrinter:
    def __init__(self, *args, header=True):
        self.__dicts = []
        self.__dicts.extend(args)
        self.__header = header

    def _get_ordered_items(self):
        all_keys = []
        all_values = []
        for d in self.__dicts:
            keys, values = zip(*d.items())
            all_keys.extend(keys)
            all_values.extend(values)
        arr_keys, arr_values = np.array(all_keys), np.array(all_values)
        indexes_sort = np.argsort(arr_keys)
        return list(arr_keys[indexes_sort]), list(arr_values[indexes_sort])

    def _get_values_ordered_by_keys(self):
        _, values = self._get_ordered_items()
        return values

    def _get_ordered_keys(self):
        keys, _ = self._get_ordered_items()
        return keys

    def add(self, d):
        self.__dicts.append(d)

    def print(self):
        headers, values = self._get_ordered_items()
        headers = [str(h) for h in headers]
        values = [str(v) for v in values]
        if self.__header:
            print(",".join(headers))
        print(",".join(values))


if __name__ == "__main__":
    paraman = ParameterManager({"a": 4})
    resulman = ResultManager({"b": 2})
    resprinter = ResultPrinter(paraman)
    resprinter.add(resulman)
    resprinter.print()
