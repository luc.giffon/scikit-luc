# import tensorflow as tftf
import keras.backend as K
from keras.activations import tanh

from skluc.main.tensorflow_.utils import replace_nan


def keras_linear_kernel(args, normalize=True):
    X = args[0]
    Y = args[1]
    if normalize:
        X = K.l2_normalize(X, axis=-1)
        Y = K.l2_normalize(Y, axis=-1)
    return K.dot(X, K.transpose(Y))


def keras_chi_square_CPD(args, epsilon=None, tanh_activation=True, normalize=True):
    X = args[0]
    Y = args[1]
    if normalize:
        X = K.l2_normalize(X, axis=-1)
        Y = K.l2_normalize(Y, axis=-1)
    # the drawing of the matrix X expanded looks like a wall
    wall = K.expand_dims(X, axis=1)
    # the drawing of the matrix Y expanded looks like a floor
    floor = K.expand_dims(Y, axis=0)
    numerator = K.square((wall - floor))
    denominator = wall + floor
    if epsilon is not None:
        quotient = numerator / (denominator + epsilon)
    else:
        quotient = numerator / denominator
    quotient_without_nan = replace_nan(quotient)
    result = - K.sum(quotient_without_nan, axis=2)
    if tanh_activation:
        return tanh(result)
    else:
        return result


def keras_chi_square_CPD_exp(X, Y, gamma):
    K = keras_chi_square_CPD(X, Y)
    K *= gamma
    return K.exp(K)

# def chi2_kernel(args):
#     x = args[0]
#     y = tf.concat(args[1:], 0)
#
#     x = tf.nn.l2_normalize(x, axis=-1)
#     y = tf.nn.l2_normalize(y, axis=-1)
#     # the drawing of the matrix X expanded looks like a wall
#     wall = tf.expand_dims(x, axis=1)
#     # the drawing of the matrix Y expanded looks like a floor
#     floor = tf.expand_dims(y, axis=0)
#     numerator = tf.square(tf.subtract(wall, floor))
#     denominator = tf.add(wall, floor) + 0.001
#     quotient = numerator / denominator
#     quotient_without_nan = quotient #replace_nan(quotient)
#
#     K = - tf.reduce_sum(quotient_without_nan, axis=2)
#     return tf.tanh(K)
