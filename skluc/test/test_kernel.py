import unittest
import tensorflow as tf
import numpy as np
from sklearn.metrics.pairwise import rbf_kernel, chi2_kernel, additive_chi2_kernel, sigmoid_kernel, laplacian_kernel, polynomial_kernel

from skluc.main.data.mldatasets.MnistDataset import MnistDataset
from skluc.main.tensorflow_.kernel import tf_rbf_kernel, tf_chi_square_CPD_exp, tf_chi_square_CPD, tf_sigmoid_kernel, \
    tf_laplacian_kernel, tf_sum_of_kernels, tf_stack_of_kernels, tf_polynomial_kernel


class TestKernel(unittest.TestCase):
    def setUp(self):
        self.val_size = 50
        mnist = MnistDataset(validation_size=self.val_size)
        mnist.load()
        mnist.normalize()
        mnist.data_astype(np.float32)
        mnist.labels_astype(np.float32)
        X_train, Y_train = mnist.train
        X_test, Y_test = mnist.test
        X_val, Y_val = mnist.validation

        self.X_val = X_val
        self.Y_val = Y_val
        self.X_train = X_train
        self.Y_train = Y_train
        self.X_test = X_test
        self.Y_test = Y_test

        self.sess = tf.InteractiveSession()

        self.custom_kernels = {
            "rbf": tf_rbf_kernel,
            "exp_chi2": tf_chi_square_CPD_exp,
            "chi2": tf_chi_square_CPD,
            "sigmoid": tf_sigmoid_kernel,
            "laplacian": tf_laplacian_kernel,
            "poly": tf_polynomial_kernel
        }

        self.custom_kernels_params = {
            "rbf": {"gamma": 0.01},
            "exp_chi2": {"gamma": 0.01},
            "chi2": {},
            "sigmoid": {"gamma": 1/self.val_size, "constant": 1.},
            "laplacian": {"gamma": 1/self.val_size}
        }

        self.sklearn_kernels = {
            "rbf": rbf_kernel,
            "exp_chi2": chi2_kernel,
            "chi2": additive_chi2_kernel,
            "sigmoid": sigmoid_kernel,
            "laplacian": laplacian_kernel,
            "poly2": polynomial_kernel
        }

        self.sklearn_kernels_params = {
            "rbf": {"gamma": 0.01},
            "exp_chi2": {"gamma": 0.01},
            "chi2": {},
            "sigmoid": {"gamma": 1 / self.val_size, "coef0": 1.},
            "laplacian": {"gamma": 1 / self.val_size},
            "poly2": {"coef0": 1, "degree":2}
        }

    def test_tf_polynomial2_kernel(self):
        coef=1
        degrees = [2, 3]

        for degree in degrees:
            print(f"Degree {degree}")
            expected_rbf_kernel = polynomial_kernel(self.X_val, self.X_val, degree=degree, coef0=coef)
            obtained_rbf_kernel = tf_polynomial_kernel(self.X_val, self.X_val, degree=degree).eval()
            difference_rbf_kernel = np.linalg.norm(expected_rbf_kernel - obtained_rbf_kernel) / (self.val_size**2)
            self.assertAlmostEqual(difference_rbf_kernel, 0, delta=1e-4)

            example1 = self.X_val[0].reshape((1, -1))
            example2 = self.X_val[1].reshape((1, -1))
            expected_rbf_kernel_value = polynomial_kernel(example1, example2, degree=degree, coef0=coef)
            obtained_rbf_kernel_value = tf_polynomial_kernel(example1, example2, degree=degree).eval()
            difference_rbf_kernel_value = np.linalg.norm(expected_rbf_kernel_value - obtained_rbf_kernel_value)
            self.assertAlmostEqual(difference_rbf_kernel_value, 0, delta=1e-4)

            expected_rbf_kernel_vector = polynomial_kernel(example1, self.X_val, degree=degree, coef0=coef)
            obtained_rbf_kernel_vector = tf_polynomial_kernel(example1, self.X_val, degree=degree).eval()
            difference_rbf_kernel_vector = np.linalg.norm(expected_rbf_kernel_vector - obtained_rbf_kernel_vector) / (self.val_size)
            self.assertAlmostEqual(difference_rbf_kernel_vector, 0, delta=1e-4)

    def test_tf_rbf_kernel(self):
        gamma = 0.01
        expected_rbf_kernel = rbf_kernel(self.X_val, self.X_val, gamma=gamma)
        obtained_rbf_kernel = tf_rbf_kernel(self.X_val, self.X_val, gamma=gamma).eval()
        difference_rbf_kernel = np.linalg.norm(expected_rbf_kernel - obtained_rbf_kernel) / (self.val_size**2)
        self.assertAlmostEqual(difference_rbf_kernel, 0, delta=1e-5)

        example1 = self.X_val[0].reshape((1, -1))
        example2 = self.X_val[1].reshape((1, -1))
        expected_rbf_kernel_value = rbf_kernel(example1, example2, gamma=gamma)
        obtained_rbf_kernel_value = tf_rbf_kernel(example1, example2, gamma=gamma).eval()
        difference_rbf_kernel_value = np.linalg.norm(expected_rbf_kernel_value - obtained_rbf_kernel_value)
        self.assertAlmostEqual(difference_rbf_kernel_value, 0, delta=1e-5)

        expected_rbf_kernel_vector = rbf_kernel(example1, self.X_val, gamma=gamma)
        obtained_rbf_kernel_vector = tf_rbf_kernel(example1, self.X_val, gamma=gamma).eval()
        difference_rbf_kernel_vector = np.linalg.norm(expected_rbf_kernel_vector - obtained_rbf_kernel_vector) / (self.val_size)
        self.assertAlmostEqual(difference_rbf_kernel_vector, 0, delta=1e-5)

    def test_tf_chi_square_CPD_kernel(self):
        expected_chi_square_cpd_kernel = additive_chi2_kernel(self.X_val, self.X_val)
        obtained_chi_square_cpd_kernel = tf_chi_square_CPD(self.X_val, self.X_val).eval()
        difference_chi_square_cpd_kernel = expected_chi_square_cpd_kernel - obtained_chi_square_cpd_kernel
        norm_of_difference = np.linalg.norm(difference_chi_square_cpd_kernel) / (self.val_size**2)
        self.assertAlmostEqual(norm_of_difference, 0, delta=1e-5)

        example1 = self.X_val[0].reshape((1, -1))
        example2 = self.X_val[1].reshape((1, -1))
        expected_chi_square_cpd_kernel = additive_chi2_kernel(example1, example2)
        obtained_chi_square_cpd_kernel = tf_chi_square_CPD(example1, example2).eval()
        difference_chi_square_cpd_kernel = np.linalg.norm(expected_chi_square_cpd_kernel - obtained_chi_square_cpd_kernel)
        self.assertAlmostEqual(difference_chi_square_cpd_kernel, 0, delta=1e-4)

        expected_chi_square_cpd_kernel = additive_chi2_kernel(example1, self.X_val)
        obtained_chi_square_cpd_kernel = tf_chi_square_CPD(example1, self.X_val).eval()
        difference_chi_square_cpd_kernel = np.linalg.norm(expected_chi_square_cpd_kernel - obtained_chi_square_cpd_kernel) / (self.val_size)
        self.assertAlmostEqual(difference_chi_square_cpd_kernel, 0, delta=1e-5)

    def test_tf_chi_square_exp_CPD_kernel(self):
        gamma = 0.01
        expected_chi_square_cpd_exp_kernel = chi2_kernel(self.X_val, self.X_val, gamma=gamma)
        obtained_chi_square_cpd_exp_kernel = tf_chi_square_CPD_exp(self.X_val, self.X_val, gamma=gamma).eval()
        difference_chi_square_cpd_exp_kernel = expected_chi_square_cpd_exp_kernel - obtained_chi_square_cpd_exp_kernel
        norm_of_difference = np.linalg.norm(difference_chi_square_cpd_exp_kernel) / (self.val_size**2)
        self.assertAlmostEqual(norm_of_difference, 0, delta=1e-5)

        example1 = self.X_val[0].reshape((1, -1))
        example2 = self.X_val[1].reshape((1, -1))
        expected_chi_square_cpd_exp_kernel = chi2_kernel(example1, example2, gamma=gamma)
        obtained_chi_square_cpd_exp_kernel = tf_chi_square_CPD_exp(example1, example2, gamma=gamma).eval()
        difference_chi_square_cpd_exp_kernel = np.linalg.norm(expected_chi_square_cpd_exp_kernel - obtained_chi_square_cpd_exp_kernel)
        self.assertAlmostEqual(difference_chi_square_cpd_exp_kernel, 0, delta=1e-5)

        expected_chi_square_cpd_exp_kernel = chi2_kernel(example1, self.X_val, gamma=gamma)
        obtained_chi_square_cpd_exp_kernel = tf_chi_square_CPD_exp(example1, self.X_val, gamma=gamma).eval()
        difference_chi_square_cpd_exp_kernel = np.linalg.norm(expected_chi_square_cpd_exp_kernel - obtained_chi_square_cpd_exp_kernel) / (self.val_size)
        self.assertAlmostEqual(difference_chi_square_cpd_exp_kernel, 0, delta=1e-5)

    def test_tf_sigmoid_kernel(self):
        gamma = 1/self.val_size
        const = 1
        expected_sigmoid_kernel = sigmoid_kernel(self.X_val, self.X_val, gamma=gamma, coef0=const)
        obtained_sigmoid_kernel = tf_sigmoid_kernel(self.X_val, self.X_val, gamma=gamma, constant=const).eval()
        difference_sigmoid_kernel = expected_sigmoid_kernel - obtained_sigmoid_kernel
        norm_of_difference = np.linalg.norm(difference_sigmoid_kernel) / (self.val_size**2)
        self.assertAlmostEqual(norm_of_difference, 0, delta=1e-5)

        example1 = self.X_val[0].reshape((1, -1))
        example2 = self.X_val[1].reshape((1, -1))
        expected_sigmoid_kernel = sigmoid_kernel(example1, example2, gamma=gamma, coef0=const)
        obtained_sigmoid_kernel = tf_sigmoid_kernel(example1, example2, gamma=gamma, constant=const).eval()
        difference_sigmoid_kernel = np.linalg.norm(expected_sigmoid_kernel - obtained_sigmoid_kernel)
        self.assertAlmostEqual(difference_sigmoid_kernel, 0, delta=1e-5)

        expected_sigmoid_kernel = sigmoid_kernel(example1, self.X_val, gamma=gamma, coef0=const)
        obtained_sigmoid_kernel = tf_sigmoid_kernel(example1, self.X_val, gamma=gamma, constant=const).eval()
        difference_sigmoid_kernel = np.linalg.norm(expected_sigmoid_kernel - obtained_sigmoid_kernel) / (self.val_size)
        self.assertAlmostEqual(difference_sigmoid_kernel, 0, delta=1e-5)

    def test_tf_laplacian_kernel(self):
        gamma = 1 / self.val_size
        expected_sigmoid_kernel = laplacian_kernel(self.X_val, self.X_val, gamma=gamma)
        obtained_sigmoid_kernel = tf_laplacian_kernel(self.X_val, self.X_val, gamma=gamma).eval()
        difference_sigmoid_kernel = expected_sigmoid_kernel - obtained_sigmoid_kernel
        norm_of_difference = np.linalg.norm(difference_sigmoid_kernel) / (self.val_size ** 2)
        self.assertAlmostEqual(norm_of_difference, 0, delta=1e-5)

        example1 = self.X_val[0].reshape((1, -1))
        example2 = self.X_val[1].reshape((1, -1))
        expected_rbf_kernel_value = laplacian_kernel(example1, example2, gamma=gamma)
        obtained_rbf_kernel_value = tf_laplacian_kernel(example1, example2, gamma=gamma).eval()
        difference_rbf_kernel_value = np.linalg.norm(expected_rbf_kernel_value - obtained_rbf_kernel_value)
        self.assertAlmostEqual(difference_rbf_kernel_value, 0, delta=1e-5)

        expected_rbf_kernel_vector = laplacian_kernel(example1, self.X_val, gamma=gamma)
        obtained_rbf_kernel_vector = tf_laplacian_kernel(example1, self.X_val, gamma=gamma).eval()
        difference_rbf_kernel_vector = np.linalg.norm(expected_rbf_kernel_vector - obtained_rbf_kernel_vector) / (self.val_size)
        self.assertAlmostEqual(difference_rbf_kernel_vector, 0, delta=1e-5)

    def _build_lst_kernel_fct_param(self):
        lst_custom_k_fct = []
        lst_custom_k_params = []
        lst_sklearn_k_fct = []
        lst_sklearn_k_params = []
        for k_name in self.custom_kernels.keys():
            custom_k_fct = self.custom_kernels[k_name]
            sklearn_k_fct = self.sklearn_kernels[k_name]
            custom_k_param = self.custom_kernels_params[k_name]
            sklearn_k_param = self.sklearn_kernels_params[k_name]

            lst_custom_k_fct.append(custom_k_fct)
            lst_custom_k_params.append(custom_k_param)

            lst_sklearn_k_fct.append(sklearn_k_fct)
            lst_sklearn_k_params.append(sklearn_k_param)

        return (lst_custom_k_fct, lst_custom_k_params,
                lst_sklearn_k_fct, lst_sklearn_k_params)

    def test_stack_of_kernels(self):
        (lst_custom_k_fct, lst_custom_k_params,
         lst_sklearn_k_fct, lst_sklearn_k_params) = self._build_lst_kernel_fct_param()

        expected_stack_of_kernel = self._sklearn_stack_of_kernels(self.X_val, self.X_val, lst_sklearn_k_fct, lst_sklearn_k_params)
        obtained_stack_of_kernel = tf_stack_of_kernels(self.X_val, self.X_val, lst_custom_k_fct, lst_custom_k_params).eval()
        difference_stack_kernel = expected_stack_of_kernel - obtained_stack_of_kernel
        norm_of_difference = np.linalg.norm(difference_stack_kernel) / (self.val_size ** 2)
        self.assertAlmostEqual(norm_of_difference, 0, delta=1e-5)

        example1 = self.X_val[0].reshape((1, -1))
        example2 = self.X_val[1].reshape((1, -1))
        expected_stack_of_kernel_value = self._sklearn_stack_of_kernels(example1, example2, lst_sklearn_k_fct, lst_sklearn_k_params)
        obtained_stack_of_kernel_value = tf_stack_of_kernels(example1, example2, lst_custom_k_fct, lst_custom_k_params).eval()
        difference_stack_of_kernel_value = np.linalg.norm(expected_stack_of_kernel_value - obtained_stack_of_kernel_value)
        self.assertAlmostEqual(difference_stack_of_kernel_value, 0, delta=1e-4)

        expected_stack_of_kernel_vector = self._sklearn_stack_of_kernels(example1, self.X_val, lst_sklearn_k_fct, lst_sklearn_k_params)
        obtained_stack_of_kernel_vector = tf_stack_of_kernels(example1, self.X_val, lst_custom_k_fct, lst_custom_k_params).eval()
        difference_stack_of_kernel_vector = np.linalg.norm(expected_stack_of_kernel_vector - obtained_stack_of_kernel_vector) / (
        self.val_size)
        self.assertAlmostEqual(difference_stack_of_kernel_vector, 0, delta=1e-5)


    def test_sum_of_kernels(self):

        (lst_custom_k_fct, lst_custom_k_params,
         lst_sklearn_k_fct, lst_sklearn_k_params) = self._build_lst_kernel_fct_param()

        expected_sum_of_kernel = self._sklearn_sum_of_kernels(self.X_val, self.X_val, lst_sklearn_k_fct, lst_sklearn_k_params)
        obtained_sum_of_kernel = tf_sum_of_kernels(self.X_val, self.X_val, lst_custom_k_fct, lst_custom_k_params).eval()
        difference_sum_kernel = expected_sum_of_kernel - obtained_sum_of_kernel
        norm_of_difference = np.linalg.norm(difference_sum_kernel) / (self.val_size ** 2)
        self.assertAlmostEqual(norm_of_difference, 0, delta=1e-5)

        example1 = self.X_val[0].reshape((1, -1))
        example2 = self.X_val[1].reshape((1, -1))
        expected_sum_of_kernel_value = self._sklearn_sum_of_kernels(example1, example2, lst_sklearn_k_fct, lst_sklearn_k_params)
        obtained_sum_of_kernel_value = tf_sum_of_kernels(example1, example2, lst_custom_k_fct, lst_custom_k_params).eval()
        difference_sum_of_kernel_value = np.linalg.norm(expected_sum_of_kernel_value - obtained_sum_of_kernel_value)
        self.assertAlmostEqual(difference_sum_of_kernel_value, 0, delta=1e-4)

        expected_sum_of_kernel_vector = self._sklearn_sum_of_kernels(example1, self.X_val, lst_sklearn_k_fct, lst_sklearn_k_params)
        obtained_sum_of_kernel_vector = tf_sum_of_kernels(example1, self.X_val, lst_custom_k_fct, lst_custom_k_params).eval()
        difference_sum_of_kernel_vector = np.linalg.norm(expected_sum_of_kernel_vector - obtained_sum_of_kernel_vector) / (
        self.val_size)
        self.assertAlmostEqual(difference_sum_of_kernel_vector, 0, delta=1e-5)

    @staticmethod
    def _sklearn_sum_of_kernels(X, Y, kernel_fcts, kernel_params):
        K_sum = np.zeros((X.shape[0], Y.shape[0]))
        i = 0
        while i < len(kernel_fcts):
            k_fct = kernel_fcts[i]
            k_param = kernel_params[i]
            K_sum += k_fct(X, Y, **k_param)
            i += 1
        return K_sum

    @staticmethod
    def _sklearn_stack_of_kernels(X, Y, kernel_fcts, kernel_params):
        results_of_kernels = []
        i = 0
        while i < len(kernel_fcts):
            k_fct = kernel_fcts[i]
            k_params = kernel_params[i]
            k_result = k_fct(X, Y, **k_params)
            results_of_kernels.append(k_result)
            i += 1
        return np.hstack(results_of_kernels)

    def tearDown(self):
        self.sess.close()


if __name__ == '__main__':
    unittest.main()
