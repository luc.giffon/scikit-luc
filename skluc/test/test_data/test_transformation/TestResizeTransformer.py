import unittest

from skluc.main.data.mldatasets.Cifar100FineDataset import Cifar100FineDataset
from skluc.main.data.mldatasets.Cifar10Dataset import Cifar10Dataset
from skluc.main.data.mldatasets.MnistDataset import MnistDataset
from skluc.main.data.mldatasets.SVHNDataset import SVHNDataset
from skluc.main.data.transformation.ResizeTransformer import ResizeTransformer
from skluc.main.utils import logger


class TestResizeTransformer(unittest.TestCase):
    def setUp(self):
        self.dict_datasets = {
            "mnist": MnistDataset,
            "cifar10": Cifar10Dataset,
            "cifar100": Cifar100FineDataset,
            "svhn": SVHNDataset
        }
        self.lst_sizes = [
            (28, 32),
            (32, 32),
            (28, 28),
            (32, 28)
        ]

    def test_transform(self):
        valsize = 10000
        for data_name in self.dict_datasets:
            logger.info("Testing dataset {}".format(data_name))
            for size in self.lst_sizes:
                logger.info("Testing size {}".format(str(size)))
                dataset = self.dict_datasets[data_name]
                d = dataset(validation_size=valsize)
                d.load()
                d.flatten()
                d.to_image()
                trans = ResizeTransformer(data_name=data_name, output_shape=size)
                d.apply_transformer(transformer=trans)
                new_size = d.train.data[0].shape[:-1]
                self.assertEqual(new_size[0], size[0])
                self.assertEqual(new_size[1], size[1])
                del trans

    def test_init(self):
        for data_name in self.dict_datasets:
            for size in self.lst_sizes:
                logger.info("Testing size {}".format(str(size)))
                trans = ResizeTransformer(data_name=data_name, output_shape=size)
                del trans


if __name__ == '__main__':
    unittest.main()
