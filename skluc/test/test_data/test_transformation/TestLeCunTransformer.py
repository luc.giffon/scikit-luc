import unittest

from skluc.main.data.mldatasets.MnistDataset import MnistDataset
from skluc.main.data.transformation.LeCunTransformer import LecunTransformer
from skluc.main.utils import logger


class TestLeCunTransformer(unittest.TestCase):
    def setUp(self):
        self.dict_datasets = {
            "mnist": MnistDataset
        }

    def test_transform(self):
        valsize = 10000
        for data_name in self.dict_datasets:
            logger.info("Testing dataset {}".format(data_name))
            dataset = self.dict_datasets[data_name]
            d = dataset(validation_size=valsize)
            d.load()
            d.flatten()
            d.to_image()
            trans = LecunTransformer(data_name=data_name)
            d.apply_transformer(transformer=trans)
            del trans

    def test_init(self):
        for data_name in self.dict_datasets:
            trans = LecunTransformer(data_name=data_name)
            del trans


if __name__ == '__main__':
    unittest.main()
