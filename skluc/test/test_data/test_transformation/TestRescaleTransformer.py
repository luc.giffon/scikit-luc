import unittest

from skluc.main.data.mldatasets.Cifar100FineDataset import Cifar100FineDataset
from skluc.main.data.mldatasets.Cifar10Dataset import Cifar10Dataset
from skluc.main.data.mldatasets.MnistDataset import MnistDataset
from skluc.main.data.mldatasets.SVHNDataset import SVHNDataset
from skluc.main.data.transformation.RescaleTransformer import RescaleTransformer
from skluc.main.utils import logger


class TestResizeTransformer(unittest.TestCase):
    def setUp(self):
        self.dict_datasets = {
            "mnist": MnistDataset,
            "cifar10": Cifar10Dataset,
            "cifar100": Cifar100FineDataset,
            "svhn": SVHNDataset
        }
        self.lst_scales = [
            0.5,
            0.7,
            1,
            2
        ]

    def test_transform(self):
        valsize = 10000
        for data_name in self.dict_datasets:
            logger.info("Testing dataset {}".format(data_name))
            for scale in self.lst_scales:
                logger.info("Testing size {}".format(str(scale)))
                dataset = self.dict_datasets[data_name]
                d = dataset(validation_size=valsize)
                d.load()
                d.flatten()
                d.to_image()
                old_size = tuple(d.train.data[0].shape[:-1])
                logger.debug("Old size of images: {}".format(old_size))
                trans = RescaleTransformer(data_name=data_name, scaling_factor=scale)
                d.apply_transformer(transformer=trans)
                new_size = tuple(d.train.data[0].shape[:-1])
                logger.debug("New size of images: {}".format(new_size))
                self.assertAlmostEqual(new_size[0] / scale, old_size[0], delta=1)
                self.assertAlmostEqual(new_size[1] / scale, old_size[1], delta=1)
                del trans

    def test_init(self):
        for data_name in self.dict_datasets:
            for scale in self.lst_scales:
                logger.info("Testing size {}".format(str(scale)))
                trans = RescaleTransformer(data_name=data_name, scaling_factor=scale)
                del trans


if __name__ == '__main__':
    unittest.main()
