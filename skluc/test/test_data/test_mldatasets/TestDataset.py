import unittest

import numpy as np

from skluc.main.data.mldatasets import Cifar100FineDataset, Cifar10Dataset, MnistDataset, SVHNDataset
from skluc.main.data.mldatasets.Dataset import Dataset
from skluc.main.utils import LabeledData


class FooDataset(Dataset):
    def __init__(self, validation_size=1000, seed=0):
        super().__init__([], "foo", validation_size=validation_size, seed=seed)

    def read(self):
        train_size = 9000
        test_size = 1000
        np.random.seed(self.seed)
        self._train = LabeledData(data=np.random.rand(train_size, 200),
                                  labels=np.random.choice(10, size=train_size, replace=True))
        self._test = LabeledData(data=np.random.rand(test_size, 200),
                                 labels=np.random.choice(10, size=test_size, replace=True))


class TestDataset(unittest.TestCase):
    def setUp(self):
        self.dataset_classes = [FooDataset,
                                Cifar100FineDataset,
                                Cifar10Dataset,
                                MnistDataset,
                                SVHNDataset]

    def test_min_max(self):
        for d_class in self.dataset_classes:
            d1 = d_class(validation_size=1000, seed=0)
            d1.load()
            mini_train = np.min(d1.train.data)
            maxi_train = np.max(d1.train.data)
            self.assertEqual(mini_train, d1.min)
            self.assertEqual(maxi_train, d1.max)
            mini_test = np.min(d1.test.data)
            maxi_test = np.max(d1.test.data)
            self.assertNotEquals(mini_test, d1.min)
            self.assertNotEquals(maxi_test, d1.max)


    def test_seed_train_val(self):
        for d_class in self.dataset_classes:
            d1 = d_class(validation_size=1000, seed=0)
            d2 = d_class(validation_size=1000, seed=0)
            d3 = d_class(validation_size=1000, seed=1)
            d1.load()
            d2.load()
            d3.load()
            self.assertTrue((d1.train.data == d2.train.data).all(),
                            msg="Same seed gives different train/val split in {} dataset".format(d_class.__name__))
            self.assertTrue((d1.train.data != d3.train.data).any(),
                            msg="Different seeds give same train/val split in {} dataset".format(d_class.__name__))

    def test_seed_uniform_rand_indices_train(self):
        size = 100
        size_bis = 102
        size2 = 120
        for d_class in self.dataset_classes:
            # test same subsample size
            # ------------------------
            d1 = d_class(validation_size=1000, seed=0)
            d2 = d_class(validation_size=1000, seed=0)
            d3 = d_class(validation_size=1000, seed=1)
            d1.load()
            d2.load()
            d3.load()
            sub_indexes1 = d1.get_uniform_class_rand_indices_train(size)
            sub_indexes1_not_modulo = d1.get_uniform_class_rand_indices_train(size_bis)
            sub_indexes2 = d2.get_uniform_class_rand_indices_train(size)
            sub_indexes3 = d3.get_uniform_class_rand_indices_train(size)
            # the returned number of index is actually equal to the one asked
            self.assertEqual(len(sub_indexes1), size)
            self.assertEqual(len(sub_indexes1_not_modulo), size_bis)
            self.assertEqual(len(sub_indexes2), size)
            self.assertEqual(len(sub_indexes3), size)

            subsample1 = d1.train.data[sub_indexes1]
            subsample2 = d2.train.data[sub_indexes2]
            subsample3 = d3.train.data[sub_indexes3]

            unique_labels = np.unique(d1.train.labels, axis=0)
            labels = d1.train.labels[sub_indexes1]
            labels_bis = d1.train.labels[sub_indexes1_not_modulo]
            for u_lab in unique_labels:
                if len(labels.shape) == 1:
                    bool_idx_labs = labels == u_lab
                    bool_idx_labs_bis = labels_bis == u_lab

                elif len(labels.shape) == 2:
                    bool_idx_labs = np.all(labels == u_lab, axis=-1)
                    bool_idx_labs_bis = np.all(labels_bis == u_lab, axis=-1)
                else:
                    raise ValueError(
                        "Function get_uniform_class_rand_indices_train has not been intended for others than scalar and vector valued labels")

                idx_labs = np.where(bool_idx_labs)[0]
                idx_labs_bis = np.where(bool_idx_labs_bis)[0]
                self.assertTrue(
                    len(idx_labs) == size // len(unique_labels) or len(idx_labs) == size // len(unique_labels) + 1)
                self.assertTrue(len(idx_labs_bis) == size // len(unique_labels) or len(idx_labs_bis) == size // len(
                    unique_labels) + 1)

            self.assertTrue((subsample1 == subsample2).all(),
                            msg="Same seed gives different subsamples in {} dataset".format(d_class.__name__))
            self.assertTrue((subsample1 != subsample3).any(),
                            msg="Different seed gives same subsamples in {} dataset".format(d_class.__name__))

            # test different subsample size
            # -----------------------------
            d1 = d_class(validation_size=1000, seed=1)
            d2 = d_class(validation_size=1000, seed=1)
            d1.load()
            d2.load()
            sub_indexes1 = d1.get_uniform_class_rand_indices_train(size)
            sub_indexes2 = d2.get_uniform_class_rand_indices_train(size2)
            self.assertEqual(len(sub_indexes1), size)
            self.assertEqual(len(sub_indexes2), size2)
            subsample1 = d1.train.data[sub_indexes1]
            subsample2 = d2.train.data[sub_indexes2]

            for subs1 in subsample1:
                found = False
                for subs2 in subsample2:
                    if (subs1 == subs2).all():
                        found = True
                        break
                if not found:
                    break

            self.assertTrue(found,
                            msg="Little subsample is not a subset of big subsample using same seed in {} dataset".format(
                                d_class.__name__))

    def test_seed_uniform_rand_indices_validation(self):
        size = 10
        size2 = 12
        for d_class in self.dataset_classes:
            # test same subsample size
            # ------------------------
            d1 = d_class(validation_size=1000, seed=0)
            d2 = d_class(validation_size=1000, seed=0)
            d3 = d_class(validation_size=1000, seed=1)
            d1.load()
            d2.load()
            d3.load()
            sub_indexes1 = d1.get_uniform_class_rand_indices_validation(size)
            sub_indexes2 = d2.get_uniform_class_rand_indices_validation(size)
            sub_indexes3 = d2.get_uniform_class_rand_indices_validation(size)
            subsample1 = d1.validation.data[sub_indexes1]
            subsample2 = d2.validation.data[sub_indexes2]
            subsample3 = d3.validation.data[sub_indexes3]
            self.assertTrue((subsample1 == subsample2).all(),
                            msg="Same seed gives different subsamples in {} dataset".format(d_class.__name__))
            self.assertTrue((subsample1 != subsample3).any(),
                            msg="Different seed gives same subsamples in {} dataset".format(d_class.__name__))

            # test different subsample size
            # -----------------------------
            d1 = d_class(validation_size=1000, seed=0)
            d2 = d_class(validation_size=1000, seed=0)
            d1.load()
            d2.load()
            sub_indexes1 = d1.get_uniform_class_rand_indices_validation(size)
            sub_indexes2 = d2.get_uniform_class_rand_indices_validation(size2)
            subsample1 = d1.validation.data[sub_indexes1]
            subsample2 = d2.validation.data[sub_indexes2]
            for subs1 in subsample1:
                found = False
                for subs2 in subsample2:
                    if (subs1 == subs2).all():
                        found = True
                        break
                if not found:
                    break

            self.assertTrue(found,
                            msg="Little subsample is not a subset of big subsample using same seed in {} dataset".format(
                                d_class.__name__))


if __name__ == "__main__":
    unittest.main()
