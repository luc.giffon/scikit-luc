import os
import unittest

from skluc.main.data.mldatasets import Cifar10Dataset
from skluc.main.utils import silentremove


class TestCifar10Dataset(unittest.TestCase):
    def setUp(self):
        self.mnist_names = [
            'batches.meta',
            'data_batch_1',
            'data_batch_2',
            'data_batch_3',
            'data_batch_4',
            'data_batch_5',
            'readme.html',
            'test_batch'
        ]
        self.download_dir = "/tmp"
        self.cifar10_dirname = os.path.join("cifar10", "cifar-10-batches-py")
        self.datadir_name = os.path.join(self.download_dir, self.cifar10_dirname)
        self.full_cifar10_names = [*map(lambda name: os.path.join(self.datadir_name, name), self.mnist_names)]

    def test_cifar10(self):
        cifar10 = Cifar10Dataset(s_download_dir=self.download_dir)
        cifar10_data = cifar10.load()
        for name in self.full_cifar10_names:
            self.assertTrue(os.path.exists(name))
        self.assertTrue(kw in cifar10_data.keys() for kw in ["train", "test"])

    def tearDown(self):
        for name in self.full_cifar10_names:
            silentremove(name)


if __name__ == "__main__":
    unittest.main()
