import os
import unittest

from skluc.main.data.mldatasets import MnistDataset


class TestMnistDataset(unittest.TestCase):

    def test_mnist(self):
        mnist = MnistDataset()
        mnist.load()
        for name in mnist.l_filepaths:
            self.assertTrue(os.path.exists(name))

    def test_to_image(self):
        mnist = MnistDataset()
        mnist.load()
        mnist.to_image()
        self.assertTrue(mnist.train.data[0].shape == (28, 28, 1))


if __name__ == "__main__":
    unittest.main()
