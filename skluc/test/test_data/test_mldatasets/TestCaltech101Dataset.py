import os
import unittest

from skluc.main.data.mldatasets.Caltech101Dataset import Caltech101Dataset


class TestCaltech101Dataset(unittest.TestCase):

    def test_caltech(self):
        d = Caltech101Dataset()
        d.load()
        for name in d.l_filepaths:
            self.assertTrue(os.path.exists(name))


if __name__ == "__main__":
    unittest.main()
