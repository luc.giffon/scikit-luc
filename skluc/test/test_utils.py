import unittest
import numpy as np

from skluc.main.utils import compute_euristic_sigma


class TestUtils(unittest.TestCase):
    def setUp(self):
        self.dataset = np.identity(1000)

    def test_compute_euristic_sigma(self):
        self.assertAlmostEqual(compute_euristic_sigma(self.dataset), 1.998)


if __name__ == '__main__':
    unittest.main()
