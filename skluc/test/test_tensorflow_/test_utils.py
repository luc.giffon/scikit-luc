import unittest
import numpy as np

from skluc.main.tensorflow_.utils import get_next_batch


class TestUtils(unittest.TestCase):
    def setUp(self):
        self.length_dataset_small = 8
        self.dim_dataset = 5

        self.dataset_small = \
            np.arange(self.length_dataset_small).reshape(self.length_dataset_small, 1) * (np.ones((self.length_dataset_small, self.dim_dataset)))
        # [[0. 0. 0. 0. 0.]
        # [1. 1. 1. 1. 1.]
        # [2. 2. 2. 2. 2.]
        # [3. 3. 3. 3. 3.]
        # [4. 4. 4. 4. 4.]]

    def test_get_next_batch_normal(self):
        """
        Scenario in which a simple slice of the total dataset is returned. At start of the dataset, inside and at the end.
        :return:
        """
        batch_size_smaller = 2

        # starting scenario
        num_batch = 0
        expected = np.arange(num_batch*batch_size_smaller, (batch_size_smaller*(1+num_batch))).reshape(batch_size_smaller, 1) * (
            np.ones((batch_size_smaller, self.dim_dataset)))
        obtained = get_next_batch(self.dataset_small, num_batch, batch_size_smaller)

        print(expected)
        print(obtained)
        self.assertTrue((expected == obtained).all())

        # inside scenario
        num_batch = 1
        expected = np.arange(num_batch*batch_size_smaller, (batch_size_smaller*(1+num_batch))).reshape(batch_size_smaller, 1) * (
            np.ones((batch_size_smaller, self.dim_dataset)))
        obtained = get_next_batch(self.dataset_small, num_batch, batch_size_smaller)

        print(expected)
        print(obtained)
        self.assertTrue((expected==obtained).all())

        # ending scenario
        num_batch = 3
        expected = np.arange(num_batch*batch_size_smaller, (batch_size_smaller*(1+num_batch))).reshape(batch_size_smaller, 1) * (
            np.ones((batch_size_smaller, self.dim_dataset)))
        obtained = get_next_batch(self.dataset_small, num_batch, batch_size_smaller)

        print(expected)
        print(obtained)
        self.assertTrue((expected == obtained).all())

    def test_get_next_batch_end_cycle_param(self):
        """
        Scenario in wich dataset_size % batch_size != 0. (End of dataset scenario cycle/nocycle)
        :return:
        """
        batch_size_smaller = 3

        # no cycling scenario
        num_batch = 2
        expected_range = np.arange(num_batch*batch_size_smaller, self.length_dataset_small)
        expected = expected_range.reshape(expected_range.shape[0], 1) * (
            np.ones((expected_range.shape[0], self.dim_dataset)))
        obtained = get_next_batch(self.dataset_small, num_batch, batch_size_smaller, circle=False)

        print(expected)
        print(obtained)
        self.assertTrue((expected == obtained).all())

        self.assertTrue((expected == obtained).all())

        # cycling scenario
        num_batch = 2
        expected_range1 = np.arange(num_batch * batch_size_smaller, self.length_dataset_small)
        expected_range2 = np.arange((num_batch + 1) * batch_size_smaller - self.length_dataset_small)
        expected_range = np.concatenate((expected_range1, expected_range2))
        expected = expected_range.reshape(expected_range.shape[0], 1) * (
            np.ones((expected_range.shape[0], self.dim_dataset)))
        obtained = get_next_batch(self.dataset_small, num_batch, batch_size_smaller, circle=True)

        print(expected)
        print(obtained)
        self.assertTrue((expected == obtained).all())

    def test_get_next_batch_big_batch_size(self):
        """
        Scenario in wich batch_size > dataset size.
        :return:
        """
        supplement = 1
        batch_size_bigger = self.length_dataset_small + supplement

        # no cycling scenario
        num_batch = 0
        expected_range = np.arange(num_batch*batch_size_bigger, self.length_dataset_small)
        expected = expected_range.reshape(expected_range.shape[0], 1) * (
            np.ones((expected_range.shape[0], self.dim_dataset)))
        obtained = get_next_batch(self.dataset_small, num_batch, batch_size_bigger, circle=False)

        print(expected)
        print(obtained)
        self.assertTrue((expected == obtained).all())

        # cycling scenario
        num_batch = 0
        expected_range1 = np.arange(num_batch*batch_size_bigger, self.length_dataset_small)
        expected_range2 = np.arange(supplement)
        expected_range = np.concatenate((expected_range1, expected_range2))
        expected = expected_range.reshape(expected_range.shape[0], 1) * (
            np.ones((expected_range.shape[0], self.dim_dataset)))
        obtained = get_next_batch(self.dataset_small, num_batch, batch_size_bigger, circle=True)

        print(expected)
        print(obtained)
        self.assertTrue((expected == obtained).all())


if __name__ == '__main__':
    unittest.main()
