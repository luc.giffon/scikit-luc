import unittest
import numpy as np
from skluc.main.tensorflow_.utils import get_next_batch


class TestNeuralNetworks(unittest.TestCase):
    def setUp(self):
        self.dumb_dataset_size = 50
        self.dumb_dataset = np.arange(self.dumb_dataset_size).reshape([-1,1])

    def test_get_next_batch(self):
        batch_size_small = int(1/13 * self.dumb_dataset_size)
        output_simple = get_next_batch(self.dumb_dataset, 0, batch_size_small)
        # test if the next batch is correctly returned when we have not reach the end of the dataset
        self.assertTrue(output_simple.all() == self.dumb_dataset[:batch_size_small].all())

        last_batch_index = int(self.dumb_dataset_size/batch_size_small)
        output_circle_small = get_next_batch(self.dumb_dataset,
                                             int(self.dumb_dataset_size/batch_size_small),
                                             batch_size_small)
        expected_output_circle_small = np.vstack((self.dumb_dataset[last_batch_index*batch_size_small:],
                                                  self.dumb_dataset[:batch_size_small-last_batch_index*batch_size_small]))
        # test if the dataset circles correctly when we have reach the end of the dataset
        self.assertTrue(output_circle_small.all() == expected_output_circle_small.all())

        output_no_circle_small = get_next_batch(self.dumb_dataset,
                                                int(self.dumb_dataset_size/batch_size_small),
                                                batch_size_small,
                                                circle=False)
        expected_output_no_circle_small = self.dumb_dataset[last_batch_index*batch_size_small:]
        # test if the dataset cut correctly the last batch when we have reach the end of the dataset and we do not
        # want to circle
        self.assertTrue(output_no_circle_small.all() == expected_output_no_circle_small.all())

        batch_size_big = int(11/10 * self.dumb_dataset_size)
        output_circle_big = get_next_batch(self.dumb_dataset, 0, batch_size_big)
        expected_output_circle_big = np.vstack((self.dumb_dataset,
                                                self.dumb_dataset[:self.dumb_dataset_size - batch_size_big]))
        # test if the batch contains the full dataset plus the begining if the batch is bigger than the dataset size
        self.assertTrue(output_circle_big.all() == expected_output_circle_big.all())

        batch_size_small_round = int(1 / 10 * self.dumb_dataset_size)
        last_batch_index = int(self.dumb_dataset_size / batch_size_small_round)-1
        output_small_round = get_next_batch(self.dumb_dataset, last_batch_index, batch_size_small_round, circle=True)
        # test if the next batch is correctly returned when we have not reach the end of the dataset
        self.assertTrue(output_small_round.all() == self.dumb_dataset[batch_size_small_round:].all())


if __name__ == '__main__':
    unittest.main()
